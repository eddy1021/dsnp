/****************************************************************************
  FileName     [ dbTable.cpp ]
  PackageName  [ db ]
  Synopsis     [ Define database Table member functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2015-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#include <iomanip>
#include <string>
#include <cctype>
#include <cassert>
#include <set>
#include <algorithm>
#include <limits.h>
#include "dbTable.h"

using namespace std;

/*****************************************/
/*          Global Functions             */
/*****************************************/
ostream& operator << (ostream& os, const DBRow& r)
{
  // TODO: to print out a row.
  // - Data are seperated by a space. No trailing space at the end.
  // - Null cells are printed as '.'
  for( size_t i = 0, n = r.size() ; i < n ; i ++ ){
    if( r[ i ] == INT_MAX ) os << ".";
    else os << r[ i ];
    os << " \n"[ i == n - 1 ];
  }
  return os;
}

ostream& operator << (ostream& os, const DBTable& t)
{
  // TODO: to print out a table
  // - Data are seperated by setw(6) and aligned right.
  // - Null cells should be left blank (printed as ' ').
  for( size_t i = 0, nr = t.nRows() ; i < nr ; i ++ ){
    for( size_t j = 0, nc = t.nCols() ; j < nc ; j ++ ){
      os << setw( 6 ) << right;
      t.printData( os , t[ i ][ j ] );
    }
    os << endl;
  }
  return os;
}

pair<int,bool> nextInt( string& in , int& _ptr ){
  int len = in.length();
  long long num = 0 , sgn = 1;
  bool gotInt = false , last = false;
  while( ++ _ptr < len ){
    if( in[ _ptr ] == ',' ) break;
    if( in[ _ptr ] == 13 ){
      last = true; break;
    }
    if( in[ _ptr ] == '-' ) sgn = -1;
    if( in[ _ptr ] >= '0' && in[ _ptr ] <= '9' )
      num = num * 10 + in[ _ptr ] - '0' , gotInt = true;
  }
  if( gotInt ) return make_pair( (int)(num * sgn) , last );
  return make_pair( INT_MAX , last );
}

ifstream& operator >> (ifstream& ifs, DBTable& t)
{
  // TODO: to read in data from csv file and store them in a table
  // - You can assume all the data of the table are in a single line.
  string in;
  getline( ifs , in );
  int ptr = -1 , len = in.length();
  DBRow _row;
  while( len > 0 && in[ len - 1 ] == 13 ) len --;
  while( ptr + 1 < len ){
    pair<int,bool> pr = nextInt( in , ptr );
    _row.addData( pr.first );
    if( pr.second ){ // end of a row
      t.addRow( _row );
      _row.reset();
    }
  }
  return ifs;
}

/*****************************************/
/*   Member Functions for class DBRow    */
/*****************************************/
void
DBRow::removeCell(size_t c)
{
  // TODO
  if( c < this->size() &&
      c >= 0 ){
    for( size_t i = c, n = this->size() - 1 ; i < n ; i ++ )
      this->_data[ i ] = this->_data[ i + 1 ];
    this->_data.pop_back();
  }
}

/*****************************************/
/*   Member Functions for struct DBSort  */
/*****************************************/
bool
DBSort::operator() (const DBRow& r1, const DBRow& r2) const
{
  // TODO: called as a functional object that compares the data in r1 and r2
  //       based on the order defined in _sortOrder
  for( size_t i = 0, n = _sortOrder.size() ; i < n ; i ++ )
    if( r1[ _sortOrder[ i ] ] != r2[ _sortOrder[ i ] ] )
      return r1[ _sortOrder[ i ] ] < r2[ _sortOrder[ i ] ];
  return false;
}

/*****************************************/
/*   Member Functions for class DBTable  */
/*****************************************/
  void
DBTable::reset()
{
  // TODO
  _table.clear();
}

  void
DBTable::addCol(const vector<int>& d)
{
  // TODO: add a column to the right of the table. Data are in 'd'.
  for( size_t i = 0, n = min( d.size() , this->nRows() ) ; i < n ; i ++ )
    this->_table[ i ].addData( d[ i ] );
  for( size_t i = d.size(), n = this->nRows() ; i < n ; i ++ )
    this->_table[ i ].addData( INT_MAX );
}

  void
DBTable::delRow(int c)
{
  // TODO: delete row #c. Note #0 is the first row.
  if( c < 0 ) return;
  if( (size_t)c < this->nRows() &&
      c >= 0 ){
    for( size_t i = c, n = this->nRows() - 1 ; i < n ; i ++ )
      this->_table[ i ] = this->_table[ i + 1 ];
    this->_table.pop_back();
  }
}

  void
DBTable::delCol(int c)
{
  // delete col #c. Note #0 is the first row.
  for (size_t i = 0, n = _table.size(); i < n; ++i)
    _table[i].removeCell(c);
}

// For the following getXXX() functions...  (except for getCount())
// - Ignore null cells
// - If all the cells in column #c are null, return NAN
// - Return "float" because NAN is a float.
float
DBTable::getMax(size_t c) const
{
  // TODO: get the max data in column #c
  if( c < 0 ||
      c >= this->nCols() ) return nan("");
  float _max = nan("");
  for( size_t i = 0, n = this->nRows() ; i < n ; i ++ )
    if( this->_table[ i ][ c ] != INT_MAX )
      if( (_max != _max) ||
          this->_table[ i ][ c ] > _max )
        _max = this->_table[ i ][ c ];
  return _max;
}

float
DBTable::getMin(size_t c) const
{
  // TODO: get the min data in column #c
  if( c < 0 ||
      c >= this->nCols() ) return nan("");
  float _min = nan("");
  for( size_t i = 0, n = this->nRows() ; i < n ; i ++ )
    if( this->_table[ i ][ c ] != INT_MAX )
      if( (_min != _min) ||
          this->_table[ i ][ c ] < _min )
        _min = this->_table[ i ][ c ];
  return _min;
}

float 
DBTable::getSum(size_t c) const
{
  // TODO: compute the sum of data in column #c
  if( c < 0 ||
      c >= this->nCols() ) return nan("");
  float _sum = 0, _cnt = 0;
  for( size_t i = 0, n = this->nRows() ; i < n ; i ++ )
    if( this->_table[ i ][ c ] != INT_MAX ){
      _sum += this->_table[ i ][ c ];
      _cnt = 1.0;
    }
  if( _cnt == 0 ) return nan("");
  return _sum / _cnt;
}

int
DBTable::getCount(size_t c) const
{
  // TODO: compute the number of distinct data in column #c
  // - Ignore null cells
  if( c < 0 ||
      c >= this->nCols() ) return -1;
  set<int> S;
  for( size_t i = 0, n = this->nRows() ; i < n ; i ++ )
    if( this->_table[ i ][ c ] != INT_MAX )
      S.insert( _table[ i ][ c ] );
  return (int)S.size();
}

float
DBTable::getAve(size_t c) const
{
  // TODO: compute the average of data in column #c
  if( c < 0 ||
      c >= this->nCols() ) return nan("");
  float _sum = 0, _cnt = 0;
  for( size_t i = 0, n = this->nRows() ; i < n ; i ++ )
    if( this->_table[ i ][ c ] != INT_MAX ){
      _sum += this->_table[ i ][ c ];
      _cnt += 1.0;
    }
  if( _cnt == 0 ) return nan("");
  return _sum / _cnt;
}

void
DBTable::sort(const struct DBSort& s)
{
  // TODO: sort the data according to the order of columns in 's'
  std::sort( _table.begin() , _table.end() , s );
}

void
DBTable::printCol(size_t c) const
{
  // TODO: to print out a column.
  // - Data are seperated by a space. No trailing space at the end.
  // - Null cells are printed as '.'
  if( c < this->nCols() &&
      c >= 0 ){
    for( size_t i = 0, n = this->nRows() ; i < n ; i ++ ){
      if( this->_table[ i ][ c ] == INT_MAX ) cout << ".";
      else cout << this->_table[ i ][ c ];
      cout << " \n"[ i == n - 1 ];
    }
  }
}

void
DBTable::printSummary() const
{
  size_t nr = nRows(), nc = nCols(), nv = 0;
  for (size_t i = 0; i < nr; ++i)
    for (size_t j = 0; j < nc; ++j)
      if (_table[i][j] != INT_MAX) ++nv;
  cout << "(#rows, #cols, #data) = (" << nr << ", " << nc << ", "
    << nv << ")" << endl;
}

