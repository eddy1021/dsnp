/****************************************************************************
  FileName     [ test.cpp ]
  PackageName  [ test ]
  Synopsis     [ Test program for simple database db ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2015-present LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/
#include <iostream>
#include "dbTable.h"

using namespace std;

extern DBTable dbtbl;

class CmdParser;
CmdParser* cmdMgr = 0; // for linking purpose

int
main(int argc, char** argv)
{
   if (argc != 2) {  // testdb <cvsfile>
      cerr << "Error: using testdb <cvsfile>!!" << endl;
      return -1;
//      exit(-1);
   }

   ifstream inf(argv[1]);

   if (!inf) {
      cerr << "Error: cannot open file \"" << argv[1] << "\"!!\n";
      return -1;
//      exit(-1);
   }

   if (dbtbl) {
      cout << "Table is resetting..." << endl;
      dbtbl.reset();
   }
   if (!(inf >> dbtbl)) {
      cerr << "Error in reading csv file!!" << endl;
      return -1;
//      exit(-1);
   }

   cout << "========================" << endl;
   cout << " Print table " << endl;
   cout << "========================" << endl;
   cout << dbtbl << endl;

   // TODO
   // Insert what you want to test here by calling DBTable's member functions
   vector<int> tv( 10 , 57 );
   dbtbl.addCol( tv );
   cout << dbtbl << endl;
   dbtbl.addRow( tv );
   cout << dbtbl << endl;
   dbtbl.delRow( 2 );
   cout << dbtbl << endl;
   dbtbl.delCol( 3 );
   cout << dbtbl << endl;
   cout << dbtbl.getData( 1 , 2 ) << endl;
   cout << dbtbl.getMax( 1 ) << endl;
   cout << dbtbl.getMin( 1 ) << endl;
   cout << dbtbl.getAve( 1 ) << endl;
   cout << dbtbl.getSum( 1 ) << endl;
   cout << dbtbl.getCount( 1 ) << endl;
   cout << dbtbl.getMax( 2 ) << endl;
   cout << dbtbl.getMin( 2 ) << endl;
   cout << dbtbl.getAve( 2 ) << endl;
   cout << dbtbl.getSum( 2 ) << endl;
   cout << dbtbl.getCount( 2 ) << endl;
   dbtbl.printSummary();

   DBSort t;
   for( size_t i = 2 ; i > 0 ; i -- )
     t.pushOrder( i );
   dbtbl.sort( t );
   cout << dbtbl << endl;
   cout << dbtbl[ 0 ] << endl;
   dbtbl.printCol( 1 );

   return 0;
}
