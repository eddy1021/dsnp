/****************************************************************************
  FileName     [ cirMgr.h ]
  PackageName  [ cir ]
  Synopsis     [ Define circuit manager ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef CIR_MGR_H
#define CIR_MGR_H

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

// TODO: Feel free to define your own classes, variables, or functions.

#include "cirGate.h"
#include "cirDef.h"

extern CirMgr *cirMgr;

#define BSIZE (1<<8)
class CirMgr{
typedef unsigned long long ULL;
 public:
  CirMgr() {
    _simLog = 0;
    patternListExSize = stap2 = stap = 0;
    gates = NULL;
    simed = false;
    srand( 514514 );
  }
  ~CirMgr() {} 

  // Access functions
  // return '0' if "gid" corresponds to an undefined gate.
  CirGate* getGate(unsigned gid) const { return mList[ gid ]; }

  // Member functions about circuit construction
  bool readCircuit(const string&);
  void buildEdge( CirGate*,CirGate*,int,int,int );
  void go( CirGate* );

  // Member functions about circuit optimization
  void sweep();
  void optimize();
  void eliminate( CirGate* );
  void replace( CirGate* , int , int , int , string );


  // Member functions about simulation
  void randomSim();
  void fileSim(ifstream&);
  void simulate( int );
  void resimulate( int , int );
  void simulateGate( CirGate* );
  void simulateAllPattern();
  void setSimLog(ofstream *logFile) { _simLog = logFile; }
  void initPattern();
  void radixSort( int , int );
  void markusefulPI( CirGate* );
  ULL myRand();

  // Member functions about fraig
  void strash();
  void printFEC() const;
  void fraig();
  void simplifyGates();
  void specialSimplifyGates( int& , int& );
  void constructSatModel( SatSolver& );
  bool proofSatPair( SatSolver& , CirGate* , CirGate* );
  bool proofSatGroup( SatSolver& , int , int );

  // Member functions about circuit reporting
  void print( string&, int& )const;
  void traverse( CirGate* , int& ) const;
  void printSummary() ;
  void printNetlist() const;
  void printPIs() const;
  void printPOs() const;
  void printFloatGates();
  void printFECPairs() const;
  void goWriteAig(CirGate*,ostream&);
  void writeAag(ostream&);
  void writeGate(ostream&, CirGate*);
  void buildfecg( int );

  // general
  void goMark( CirGate* );
  void goCount( CirGate* );
  void markReachableGates();
  void TopoSort();
  bool equal( CirGate* , CirGate* );

 private:
  static bool cmp( pair<CirGate*,bool> p1 , pair<CirGate*,bool> p2 ){
    if( p1.first->magicResult != p2.first->magicResult )
      return p1.first->magicResult < p2.first->magicResult ;
    return p1.first->topoId < p2.first->topoId;
  }
  static bool cmp2( int n1 , int n2 ){ return abs( n1 ) < abs( n2 ); }
  inline ULL magicTrans( ULL vl ){ return std::min( vl , ~vl ); }
  ofstream           *_simLog;
  int nM , nI , nL , nO , nA;
  int usdA;
typedef vector<CirGate*> GateList;
  GateList iList, oList, aList;
  GateList mList;
  vector<int> floating, notusd;
  pair<CirGate*,bool> *gates;
  int _n;
  vector< vector< ULL > > patternList;
  vector< ULL > patternListEx;
  int patternListExSize;
  vector< string > outputList;
  vector< string > spatternList;

  int countI , countA , maxId;
  int ln , stap , stap2 ,  _g , _gr , _sgr , siml , elicount;
  bool simed , resim;
  vector< pair<CirGate*,bool> > buckets[ 2 ][ BSIZE ];

#define K 7
  int sPIid[ K + K ] , sPItop;
};

#endif // CIR_MGR_H
