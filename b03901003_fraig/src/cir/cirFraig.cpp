/****************************************************************************
  FileName     [ cirFraig.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define cir FRAIG functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2012-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#include <cassert>
#include <queue>
#include <math.h>
#include <algorithm>
#include "cirMgr.h"
#include "cirGate.h"
// #include "cirFraig.h"
#include "sat.h"
#include "myHashMap.h"
#include "util.h"

using namespace std;

// TODO: Please keep "CirMgr::strash()" and "CirMgr::fraig()" for cir cmd.
//       Feel free to define your own variables or functions

/*******************************/
/*   Global variable and enum  */
/*******************************/

/**************************************/
/*   Static varaibles and functions   */
/**************************************/

/*******************************************/
/*   Public member functions about fraig   */
/*******************************************/
// _floatList may be changed.
// _unusedList and _undefList won't be changed

class HashKey{
#define BS 13131ll
#define MOD 1000000007ll
 public:
  HashKey() {}
  HashKey( CirGate* t ) : tGate( t ) {}
  size_t value() const{
    if( !tGate ) return 0;
typedef long long LL;
    LL _key = 0;
    LL num[ 9 ] , nt = 0;
    for( size_t i = 0 ; i < tGate->fanIn.size() ; i ++ )
      num[ nt ++ ] = ( tGate->idIn[ i ] << 1 ) + tGate->inv[ i ];
    sort( num , num + nt );
    for( int i = 0 ; i < nt ; i ++ )
      _key = ( _key * BS + num[ i ] ) % MOD;
    return (size_t)_key;
  }
  size_t operator() () const { return value(); }
  bool operator == (const HashKey& k) const { return value() == k(); }
 private:
  CirGate* tGate;
};

bool
CirMgr::equal( CirGate* g1 , CirGate* g2 ){
  int num1[ 9 ] , n1 = 0;
  int num2[ 9 ] , n2 = 0;
  for( size_t i = 0 ; i < g1->fanIn.size() ; i ++ )
    num1[ n1 ++ ] = g1->idIn[ i ] * 2 + g1->inv[ i ];
  for( size_t i = 0 ; i < g2->fanIn.size() ; i ++ )
    num2[ n2 ++ ] = g2->idIn[ i ] * 2 + g2->inv[ i ];
  if( n1 != n2 ) return false;
  sort( num1 , num1 + n1 );
  sort( num2 , num2 + n2 );
  for( int i = 0 ; i < n1 ; i ++ )
    if( num1[ i ] != num2[ i ] )
      return false;
  return true;
}

void
CirMgr::strash(){
#define MAGIC 102199
  HashMap< HashKey,CirGate* > hashTable( MAGIC );
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    mList[ i ]->indeg = 0;
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] && mList[ i ]->reach )
      for( size_t j = 0 ; j < mList[ i ]->fanOut.size() ; j ++ )
        if( mList[ i ]->fanOut[ j ] &&
            mList[ i ]->fanOut[ j ]->reach )
          mList[ i ]->fanOut[ j ]->indeg ++;
  queue< CirGate* > Q;
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] && mList[ i ]->indeg == 0 )
      Q.push( mList[ i ] );
  while( Q.size() ){
    CirGate* nGate = Q.front(); Q.pop();
    if( !nGate->reach ) continue;
    // printf( "%d %d\n" , nGate->getId() , nGate->getType() );
    if( nGate->getType() == 3 ){
      HashKey hk( nGate );
      size_t magicValue = hk();
      size_t magicBucket = magicValue % MAGIC;
      bool good = true;
      for( size_t j = 0 ; j < hashTable[ magicBucket ].size() ; j ++ )
        if( hk == hashTable[ magicBucket ][ j ].first &&
            equal( nGate , hashTable[ magicBucket ][ j ].second ) ){
          good = false;
          replace( nGate , hashTable[ magicBucket ][ j ].second->getId() , 0 , nGate->getId() , "Strashing" );
          break;
        }
      if( good ) hashTable.forceInsert( hk , nGate );
    }
    for( size_t i = 0 ; i < nGate->fanOut.size() ; i ++ )
      if( nGate->fanOut[ i ] &&
          nGate->fanOut[ i ]->reach ){
        nGate->fanOut[ i ]->indeg --;
        if( nGate->fanOut[ i ]->indeg == 0 )
          Q.push( nGate->fanOut[ i ] );
      }
  }
}

void
CirMgr::constructSatModel( SatSolver& solver ){
  solver.initialize();
  // set variables
  mList[ 0 ]->varId = solver.newVar();
  for( size_t i = 1 ; i < mList.size() ; i ++ )
    if( mList[ i ] &&
        ( mList[ i ]->reach ||
          mList[ i ]->getType() == 1 ) &&
        mList[ i ]->getType() != 2 ){
      mList[ i ]->varId = solver.newVar();
    }
  // set aig gates
  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( aList[ i ] && aList[ i ]->reach ){
      Var vi = aList[ i ]->varId, va , vb;
      // if( aList[ i ]->fanIn[ 0 ] && aList[ i ]->fanIn[ 0 ]->getType() )
        va = aList[ i ]->fanIn[ 0 ]->varId;
      // else va = mList[ 0 ]->varId;
      // if( aList[ i ]->fanIn[ 1 ] && aList[ i ]->fanIn[ 1 ]->getType() )
        vb = aList[ i ]->fanIn[ 1 ]->varId;
      // else vb = mList[ 0 ]->varId;
      solver.addAigCNF( vi , va , aList[ i ]->inv[ 0 ] ,
                             vb , aList[ i ]->inv[ 1 ] );
    }
}

bool
CirMgr::proofSatPair( SatSolver& solver , CirGate* g1 , CirGate* g2 ){
  bool Sign = g1->result != g2->result;
  Var v1 = g1->varId;
  Var v2 = g2->varId;
  Var vf = solver.newVar();
  solver.addXorCNF( vf , v1 , false , v2 , Sign );
  solver.assumeRelease();
  solver.assumeProperty( mList[ 0 ]->varId , false );
  solver.assumeProperty( vf , true );
  return solver.assumpSolve();
}

bool
CirMgr::proofSatGroup( SatSolver& solver , int l , int r ){
  Var *vf;
  vf = new Var[ ( r - l ) * ( r - l - 1 ) / 2 ];
  int xx = 0;
  for( int i = l ; i < r ; i ++ )
    for( int j = i + 1 ; j < r ; j ++ ){
      vf[ xx ] = solver.newVar();
      solver.addXorCNF( vf[ xx ] , gates[ i ].first->varId , false ,
                                   gates[ j ].first->varId ,
                                   gates[ i ].first->result != 
                                   gates[ j ].first->result );
      xx ++;
    }
  Var last = solver.newVar();
  solver.addAigCNF( last , vf[ 0 ] , true , vf[ 1 ] , true );
  for( int i = 2 ; i < xx ; i ++ ){
    Var tmp = solver.newVar();
    solver.addAigCNF( tmp , vf[ i ] , true , last , false );
    last = tmp;
  }
  solver.assumeRelease();
  solver.assumeProperty( mList[ 0 ]->varId , false );
  solver.assumeProperty( last , false );
  return solver.assumpSolve();
}

#define SMALL 7
#define DOALL false
#define XXX 2000
void
CirMgr::fraig(){
  if( _g == 0 ) return;
  randomSim();
  simulateAllPattern();
  int LUCKY = 7;
  strash();
  SatSolver solver;
  constructSatModel( solver );
  elicount = 0;
  int cc;
  int _og = _g >> 7;
  do{
    if( _g && elicount >= 10000 ){
      constructSatModel( solver );
      elicount = 0;
    }
    cc = 0;
    int sal = _g , sar = 0;
    if( _g < _og ) break;
    for( int al = 0 , ar = 0 ; al < _g ; al = ar ){
      ar = al + 1;
      while( ar < _g && !gates[ ar ].second ) ar ++;
      // if( gates[ al ].first->getId() == 0 &&
          // ar != _g ) continue;
      int nsz = ar - al;
      if( nsz <= 1 ) continue;
      if( nsz >= 3 && nsz <= 7 ){
        if( !proofSatGroup( solver , al , ar ) ){
          for( int i = al + 1 ; i < ar ; i ++ ){
            replace( gates[ i ].first , gates[ al ].first->getId() ,
                     gates[ i ].first->result !=
                     gates[ al ].first->result ,
                     gates[ i ].first->getId() , "Fraig" );
            gates[ i ].first->magicResult += i;
            gates[ i ].second = true;
          }
        }else{
          ++ cc;
          // printf( "%d %d\n" , gates[ j ].first->getId(),
                              // gates[ k ].first->getId() );
          int pp = patternListExSize % LUCKY;
          for( size_t _ = 0 ; _ < iList.size() ; _ ++ ){
            int vl = solver.getValue( iList[ _ ]->varId );
            if( vl == -1 ) vl = rand() & 1;
            if( pp == 0 ) patternListEx[ _ ] = 0;
            if( vl ) patternListEx[ _ ] |= ( 1llu << ( pp & 63 ) );
          }
          patternListExSize ++;
          _n = patternListExSize % LUCKY;
          if( _n == 0 ) _n = LUCKY;
          if( cc == LUCKY ) break;
        }
        continue;
      }
      if( _g >= 2000 && nsz < SMALL && ar < ( _g >> 1 ) ){
        continue;
      }
      int gbound = std::max( (int)sqrt( nsz ) , 20 );
      int gcount = 0;
      if( nsz < SMALL || _g < 500 ) gbound = 1000000000;
      for( int j = al + 1 ; j < ar ; j ++ ){
        int lst = -1;
        for( int k = al ; k < j ; k ++ ){
          lst = k;
          if( !gates[ k ].first->reach ) continue;
          if( ++ gcount >= gbound ) break;
          bool Sign = gates[ j ].first->result !=
                      gates[ k ].first->result;
          if( !proofSatPair( solver , gates[ j ].first , gates[ k ].first ) ){
            // id1 must equal id2
            replace( gates[ j ].first , gates[ k ].first->getId() ,
                     Sign , gates[ j ].first->getId() , "Fraig" );
            nsz --;
            break;
          }else{
            if( DOALL ) continue;
            // id1 not equal id2
            if( nsz == 2 ){
              gates[ j ].first->magicResult ^= 1llu;
              gates[ j ].second = true;
              continue;
            }
            // if( ar - al <= SMALL ) continue;
            sal = std::min( sal , al );
            sar = std::max( sar , ar );
            ++ cc;
            // printf( "%d %d\n" , gates[ j ].first->getId(),
                                // gates[ k ].first->getId() );
            int pp = patternListExSize % LUCKY;
            for( size_t _ = 0 ; _ < iList.size() ; _ ++ ){
              int vl = solver.getValue( iList[ _ ]->varId );
              if( vl == -1 ) vl = rand() & 1;
              if( pp == 0 ) patternListEx[ _ ] = 0;
              if( vl ) patternListEx[ _ ] |= ( 1llu << ( pp & 63 ) );
            }
            patternListExSize ++;
            _n = patternListExSize % LUCKY;
            if( _n == 0 ) _n = LUCKY;
            if( cc == LUCKY ) break;
          }
        }
        if( j == ar - 1 && lst == j - 1 ){
          // all different
          for( int _ = al ; _ < ar ; _ ++ ){
            gates[ _ ].first->magicResult += _;
            gates[ _ ].second = true;
          }
        }
        if( ++ gcount >= gbound ) break;
        if( cc == LUCKY ) break;
      }
      if( cc == LUCKY ) break;
    }
    if( DOALL ) break;
    // simulate( 1 );
    if( _g < XXX ) simulateAllPattern();
    resimulate( sal , sar );
  }while( _g > 0 );
  strash();
}

void
CirMgr::specialSimplifyGates( int& fal , int& far ){
  int _sz_ = 0; _gr = _sgr = 0;
  int nal = 0 , nar = 0;
  int zsz = 0;
  for( int al = 0 , ar = 0 ; al < _g ; al = ar )
    if( gates[ al ].second ){
      ar = al + 1;
      while( ar < _g && !gates[ ar ].second ) ar ++;
      if( gates[ al ].first->getId() == 0 )
        zsz = ar - al;
      if( ar - al <= 1 ) continue;
      if( ar - al < SMALL ) _sgr ++;
      int _ssz = _sz_;
      for( int j = al ; j < ar ; j ++ )
        if( gates[ j ].first->reach ||
            gates[ j ].first->getType() != 3 )
          gates[ _sz_ ++ ] = gates[ j ];
      if( al == fal ) nal = _ssz;
      if( ar == far ) nar = _sz_;
      if( _ssz != _sz_ ){
        gates[ _ssz ].second = true; _gr ++;
        for( int j = _ssz + 1 ; j < _sz_ ; j ++ )
          if( gates[ j     ].first->magicResult !=
              gates[ j - 1 ].first->magicResult )
            gates[ j ].second = true, _gr ++;
          else gates[ j ].second = false;
      }
    }
  if( zsz >= 500 && _sz_ && gates[ 0 ].first->getId() == 0 ){
    int al = 0, ar = 1;
    while( ar < _sz_ && !gates[ ar ].second ) ar ++;
    int _ssz = 0;
    for( int i = al ; i < ar ; i ++ )
      if( gates[ i ].first->zflag != 3 ){
        gates[ _ssz ] = gates[ i ];
        if( _ssz == 0 ) gates[ _ssz ].second = true;
        else gates[ _ssz ].second = false;
        _ssz ++;
      }
    if( _ssz != ar ){
      int _sar = _ssz;
      for( int i = al ; i < ar ; i ++ )
        if( gates[ i ].first->zflag == 3 ){
          gates[ _ssz ] = gates[ i ];
          if( _ssz == _sar ) gates[ _ssz ].second = true;
          else gates[ _ssz ].second = false;
          _ssz ++;
        }
      for( int i = ar ; i < _sz_ ; i ++ )
        gates[ _ssz ++ ] = gates[ i ];
      _sz_ = _ssz;
    }
  }
  _g = _sz_;
  fal = nal; far = nar;
  fal = 0; far = _g;
}
void
CirMgr::simplifyGates(){
  int _sz_ = 0 , zsz = 0;
  _gr = _sgr = 0;
  for( int al = 0 , ar = 0 ; al < _g ; al = ar )
    if( gates[ al ].second ){
      ar = al + 1;
      while( ar < _g && !gates[ ar ].second ) ar ++;
      if( gates[ al ].first->getId() == 0 )
        zsz = ar - al;
      if( ar - al <= 1 ) continue;
      if( ar - al < SMALL ) _sgr ++;
      int _ssz = _sz_;
      for( int j = al ; j < ar ; j ++ )
        if( gates[ j ].first->reach ||
            gates[ j ].first->getType() != 3 )
          gates[ _sz_ ++ ] = gates[ j ];
      if( _ssz != _sz_ ){
        gates[ _ssz ].second = true; _gr ++;
        for( int j = _ssz + 1 ; j < _sz_ ; j ++ )
          if( gates[ j     ].first->magicResult !=
              gates[ j - 1 ].first->magicResult )
            gates[ j ].second = true, _gr ++;
          else gates[ j ].second = false;
      }
    }
  if( zsz >= 500 && _sz_ && gates[ 0 ].first->getId() == 0 ){
    int al = 0, ar = 1;
    while( ar < _sz_ && !gates[ ar ].second ) ar ++;
    int _ssz = 0;
    for( int i = al ; i < ar ; i ++ )
      if( gates[ i ].first->zflag != 3 ){
        gates[ _ssz ] = gates[ i ];
        if( _ssz == 0 ) gates[ _ssz ].second = true;
        else gates[ _ssz ].second = false;
        _ssz ++;
      }
    if( _ssz != ar ){
      int _sar = _ssz;
      for( int i = al ; i < ar ; i ++ )
        if( gates[ i ].first->zflag == 3 ){
          gates[ _ssz ] = gates[ i ];
          if( _ssz == _sar ) gates[ _ssz ].second = true;
          else gates[ _ssz ].second = false;
          _ssz ++;
        }
      for( int i = ar ; i < _sz_ ; i ++ )
        gates[ _ssz ++ ] = gates[ i ];
      _sz_ = _ssz;
    }
  }
  _g = _sz_;
}

/********************************************/
/*   Private member functions about fraig   */
/********************************************/
