/****************************************************************************
  FileName     [ cirSim.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define cir simulation functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#include <queue>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cassert>
#include "cirMgr.h"
#include "cirGate.h"
#include "util.h"

using namespace std;

// TODO: Keep "CirMgr::randimSim()" and "CirMgr::fileSim()" for cir cmd.
//       Feel free to define your own variables or functions

/*******************************/
/*   Global variable and enum  */
/*******************************/

/**************************************/
/*   Static varaibles and functions   */
/**************************************/

/************************************************/
/*   Public member functions about Simulation   */
/************************************************/

#define K 7

typedef unsigned long long ULL;

void
CirMgr::simulateGate( CirGate* nGate ){
  if( nGate->stap == stap ) return;
  nGate->stap = stap;
  if( nGate->getType() == 4 || nGate->getType() == 0 ){
    nGate->result = nGate->magicResult = 0llu;
    return;
  }
  if( nGate->getType() == 1 ){
    int i = nGate->piId;
    if( resim )
      iList[ i ]->result = patternListEx[ i ];
    else
      iList[ i ]->result = patternList[ i ][ siml >> 6 ];
    if( ln != 0 && ln != 64 )
      iList[ i ]->result |= ( myRand() << ln );
    iList[ i ]->magicResult = magicTrans( iList[ i ]->result );
    return;
  }
  ULL nn[ 2 ] , nt = 0;
  for( size_t i = 0 ; i < nGate->fanIn.size() ; i ++ ){
    simulateGate( nGate->fanIn[ i ] );
    if( !nGate->fanIn[ i ] ) nn[ nt ++ ] = 0llu;
    else if( nGate->inv[ i ] )
      nn[ nt ++ ] = ~( nGate->fanIn[ i ]->result );
    else
      nn[ nt ++ ] = nGate->fanIn[ i ]->result;
    if( nn[ nt - 1 ] == 0llu ) break;
  }
  if( nt == 0 ) nGate->result = 0llu;
  else if( nt == 1 ) nGate->result = nn[ 0 ];
  else nGate->result = nn[ 0 ] & nn[ 1 ];
  nGate->magicResult = magicTrans( nGate->result );
  if( nGate->result ==  0llu ) nGate->zflag |= 1;
  if( nGate->result == ~0llu ) nGate->zflag |= 2;
}

void
CirMgr::resimulate( int fal , int far ){
  if( _g == 0 ) return;
  resim = true; 

  specialSimplifyGates( fal , far );
  if( fal >= far ) fal = 0 , far = _g;
  // fal, far will be modified

  if( _g == 0 ) return;

  int _m = 8 * sizeof( ULL );


  bool go = true;
  for( int l = 0  ; l < _n && go ; l += _m ){
    int r = std::min( _n , l + _m );
    go = false; ln = r - l; stap ++; siml = l;

    // for fec group
    for( int al = fal , ar = fal ; al < far ; al = ar )
      if( gates[ al ].second ){
        ar = al + 1;
        while( ar < far && !gates[ ar ].second ) ar ++;
        if( ar - al <= 1 ) continue;
        for( int i = al ; i < ar ; i ++ )
          simulateGate( gates[ i ].first );
        radixSort( al , ar );
        // sort( gates + al , gates + ar , cmp );
        gates[ al ].second = true;
        for( int i = al + 1 ; i < ar ; i ++ )
          if( gates[ i ].first->magicResult !=
              gates[ i - 1 ].first->magicResult )
            gates[ i ].second = true;
          else{
            gates[ i ].second = false;
            go = true;
          }
      }
  }
  simplifyGates();
  if( _g == 0 ) return;
  cout << _n << " patterns simulated." << endl;
}

// directly simulate
void
CirMgr::simulateAllPattern(){
  if( _g == 0 ) return;
  siml = 0;
  for( int al = 0 , ar = 0 ; al < _g ; al = ar )
    if( gates[ al ].second ){
      ar = al + 1;
      while( ar < _g && !gates[ ar ].second ) ar ++;
      if( ar - al <= 1 ) continue;
      if( ar - al >= 200 ) continue;
      ++ stap2; sPItop = 0;

      for( int i = al ; i < ar && sPItop < K ; i ++ )
        markusefulPI( gates[ i ].first );
      if( sPItop < K ){ // test all input

        for( ULL k = 0 ; k < ( 1llu << sPItop ) ; k ++ ){
          for( ULL ii = 0 ; ii < (ULL)sPItop ; ii ++ ){
            if( ( k >> ii ) & 1llu )
              patternListEx[ sPIid[ ii ] ] |=   ( 1llu << k );
            else
              patternListEx[ sPIid[ ii ] ] &= (~( 1llu << k ));
          }
        }
        ++ stap; ln = ( 1llu << sPItop );

        for( int i = al ; i < ar ; i ++ )
          simulateGate( gates[ i ].first );

        radixSort( al , ar );
        gates[ al ].second = true;
        int preGood = al;
        for( int i = al + 1 ; i < ar ; i ++ ){
          if( gates[ i ].first->magicResult !=
              gates[ i - 1 ].first->magicResult )
            preGood = i;
          else
            replace( gates[ i ].first ,
                     gates[ preGood ].first->getId() ,
                     gates[ i ].first->result !=
                     gates[ preGood ].first->result ,
                     gates[ i ].first->getId() ,
                     "Fraig" );
          gates[ i ].second = true;
        }
        for( int i = al ; i < ar ; i ++ )
          gates[ i ].first->magicResult += i;
      }
    }
  simplifyGates();
}

#define SMALL 5
void
CirMgr::simulate( int kind ){
// 0 for defualt 1 for resimulate
  resim = false;

  if( !simed ){
    simed = true;
    int _sz = 0;
    for( size_t i = 0 ; i < aList.size() ; i ++ )
      if( aList[ i ] && aList[ i ]->reach )
        _sz ++;
    gates = new pair<CirGate*,bool>[ _sz + 1 ];
    gates[ 0 ] = make_pair( mList[ 0 ] , true );
    _sz = 1;
    for( size_t i = 0 ; i < aList.size() ; i ++ )
      if( aList[ i ] && aList[ i ]->reach )
        gates[ _sz ++ ] = make_pair( aList[ i ] , false );
    _g = _sz;
  }else simplifyGates();

  int _m = sizeof( ULL ) << 3;

  if( _simLog ){
    outputList.clear();
    for( int i = 0 ; i < _n ; i ++ )
      outputList.push_back( "" );
  }

  bool go = true;
  for( int l = 0 ; l < _n && go ; l += _m ){
    int r = std::min( _n , l + _m );
    go = false; ln = r - l; stap ++; siml = l;

    // for fec group
    for( int al = 0 , ar = 0 ; al < _g ; al = ar )
      if( gates[ al ].second ){
        ar = al + 1;
        while( ar < _g && !gates[ ar ].second ) ar ++;
        // if( ar - al <= SMALL ) continue;
        for( int i = al ; i < ar ; i ++ )
          simulateGate( gates[ i ].first );
        radixSort( al , ar );
        // sort( gates + al , gates + ar , cmp );
        gates[ al ].second = true;
        for( int i = al + 1 ; i < ar ; i ++ )
          if( gates[ i ].first->magicResult !=
              gates[ i - 1 ].first->magicResult )
            gates[ i ].second = true;
          else{
            go = true;
            gates[ i ].second = false;
          }
      }
    if( go ) simplifyGates();
    if( _g == 0 ) go = false;

    // write to the output
    if( _simLog ){
      for( size_t i = 0 ; i < oList.size() ; i ++ ){
        simulateGate( oList[ i ] );
        for( int j = l ; j < r ; j ++ )
          if( ( oList[ i ]->result >> ( j - l ) ) & 1llu )
            outputList[ j ] += "1";
          else outputList[ j ] += "0";
      }
      go = true;
    }
  }
  simplifyGates();
  
  cout << _n << " patterns simulated." << endl;

  if( _simLog ){
    for( int i = 0 ; i < _n ; i ++ )
      *_simLog << spatternList[ i ] << " " <<
                   outputList[ i ] << endl;
  }
}

void
CirMgr::initPattern(){
  if( patternList.size() != iList.size() ){
    patternList.resize( iList.size() );
    patternListEx.resize( iList.size() );
  }
}

ULL
CirMgr::myRand(){
  ULL ___ = 0llu;
  for( int oo = 0 ; oo < 4 ; oo ++ ){
    ___ <<= 16;
    ___ |= ( rand() & ( ( 1llu << 16 ) - 1 ) );
  }
  return ___;
}

#define MAGIC 16
void
CirMgr::randomSim(){
  initPattern();
  _n = 64 * MAGIC;
  for( int i = 0 ; i < nI ; i ++ ){
    patternList[ i ].clear();
    for( int _ = 0 ; _ < MAGIC ; _ ++ )
      patternList[ i ].push_back( myRand() );
    patternList[ i ][ 0 ] |=  2llu;
    patternList[ i ][ 0 ] &= ~1llu;
  }
  simulate( 0 );
}

void
CirMgr::fileSim(ifstream& patternFile){
  initPattern();
  spatternList.clear();
  _n = 0;
  string pat;
  while( patternFile >> pat ){
    int len = pat.length();
    if( len != nI ){
      cerr << "Error: Pattern(" << pat << ") length(" << len << ")";
      cerr << " does not match the number of inputs(" << nI << ") in a circuit!!" << endl;
      return;
    }
    for( int i = 0 ; i < len ; i ++ )
      if( pat[ i ] != '0' && pat[ i ] != '1' ){
        cerr << "Error: Pattern(" << pat << ") contains a non-0/1 character('" << pat[ i ] << "')." << endl;
        return;
      }
    spatternList.push_back( pat );
    _n ++;
  }
  for( size_t i = 0 ; i < iList.size() ; i ++ ){
    patternList[ i ].resize( ( ( _n - 1 ) >> 6 ) + 1 );
    for( size_t j = 0 ; j < patternList[ i ].size() ; j ++ )
      patternList[ i ][ j ] = 0llu;
  }
  for( int i = 0 ; i < _n ; i ++ )
    for( size_t j = 0 ; j < iList.size() ; j ++ )
      if( spatternList[ i ][ j ] == '1' )
        patternList[ j ][ i >> 6 ] |= ( 1llu << ( i & 63 ) );
  simulate( 0 );
}

#define MagicSize (1<<8)
#define MagicMask ( MagicSize - 1 )
void
CirMgr::radixSort( int al , int ar ){
  if( ar - al <= ( MagicSize << 2 ) ){
    sort( gates + al , gates + ar , cmp );
    return;
  }
  for( int i = al ; i < ar ; i ++ )
    buckets[ 0 ][ gates[ i ].first->magicResult & MagicMask ].push_back( gates[ i ] );
  for( int i = 1 , sft = 8 ; i < 8 ; i ++ , sft += 8 ){
    int now = i & 1 , pre = 1 - now;
    for( int j = 0 ; j < MagicSize ; j ++ ){
      for( size_t k = 0 ; k < buckets[ pre ][ j ].size() ; k ++ ){
        CirGate* tcg = buckets[ pre ][ j ][ k ].first;
        buckets[ now ][ ( tcg->magicResult >> sft ) & MagicMask ].push_back(
          buckets[ pre ][ j ][ k ] );
      }
      buckets[ pre ][ j ].clear();
    } 
  }
  int _a_ = al;
  for( int i = 0 ; i < MagicSize ; i ++ ){
    for( size_t j = 0 ; j < buckets[ 1 ][ i ].size() ; j ++ )
      gates[ _a_ ++ ] = buckets[ 1 ][ i ][ j ];
    buckets[ 1 ][ i ].clear();
  }
}

// mark PIs
void
CirMgr::markusefulPI( CirGate* nGate ){
  if( nGate->stap2 == stap2 ) return;
  if( sPItop > K ) return;
  nGate->stap2 = stap2;
  if( nGate->getType() == 1 ){
    sPIid[ sPItop ++ ] = nGate->piId;
    return;
  }
  if( nGate->getType() == 3 ){
    for( size_t i = 0 ; i < nGate->fanIn.size() ; i ++ )
      markusefulPI( nGate->fanIn[ i ] );
  }
}

/*************************************************/
/*   Private member functions about Simulation   */
/*************************************************/
