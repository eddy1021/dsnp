for i in {1..5}
do
  echo $i" processing..."
  time ./fraig -F "dofiles/doFraig"$i
  time ./ref/fraig -F "dofiles/testFraig"$i > "fraigResult/"$i".result"
done
# for i in {14..15}
# do
  # echo $i" processing..."
  # time ./fraig -F "dofiles/doFraig"$i
  # time ./ref/fraig -F "dofiles/testFraig"$i > "fraigResult/"$i".result"
# done
