for i in {1..15}
do
  echo $i" processing..."
  time ./fraig -F "dofiles/doFec"$i > "fecOutput/fec"$i".out"
  time ./ref/fraig -F "dofiles/doFec"$i > "fecOutput/ref_fec"$i".out"
done
