#include <bits/stdc++.h>
using namespace std;
int main(){
  cout << "Input the file name: ";
  string fname;
  cin >> fname;
  ifstream in( fname.c_str() );
  if( !in.is_open() ){
    cout << "can't open file!" << endl;
    return 0;
  }
  string ss;
  bool got = false;
  int num = 0;
  bool flag = false;
  while( getline( in , ss ) && !got ){
    int len = ss.length();
    if( !flag ){
      for( int i = 0 ; i < len - 3 ; i ++ )
        if( ss[ i ] == 'S' &&
            ss[ i + 1 ] == 't' &&
            ss[ i + 2 ] == 'a' &&
            ss[ i + 3 ] == 't' ){
          flag = true;
          break;
        }
    }
    if( flag ){
      for( int i = 0 ; i < len - 2 ; i ++ )
        if( ss[ i ] == 'A' &&
            ss[ i + 1 ] == 'I' &&
            ss[ i + 2 ] == 'G' ){
          for( int j = i + 4 ; j < len ; j ++ )
            if( ss[ j ] >= '0' && ss[ j ] <= '9' ){
              num *= 10;
              num += ( ss[ j ] - '0' );
              got = true;
            }
          if( got ) break;
        }
    }
  }
  if( got && num == 0 )
    cout << "Circuit is equivalent" << endl;
  else if( !got )
    cout << "Not found aig information" << endl;
  else 
    cout << "Circuit isn't equivalent!!" << endl;
}
