/****************************************************************************
  FileName     [ cmdReader.cpp ]
  PackageName  [ cmd ]
  Synopsis     [ Define command line reader member functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2007-2014 LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/
#include <cassert>
#include <cstring>
#include "cmdParser.h"

using namespace std;

//----------------------------------------------------------------------
//    Extrenal funcitons
//----------------------------------------------------------------------
void mybeep();
char mygetc(istream&);
ParseChar getChar(char, istream&);


//----------------------------------------------------------------------
//    Member Function for class Parser
//----------------------------------------------------------------------
  void
CmdParser::readCmd()
{
  if (_dofile.is_open()) {
    readCmdInt(_dofile);
    _dofile.close();
  }
  else
    readCmdInt(cin);
}


  void
CmdParser::readCmdInt(istream& istr)
{
  resetBufAndPrintPrompt();

  while (1) {
    char ch = mygetc(istr);
    ParseChar pch = getChar(ch, istr);
    if (pch == INPUT_END_KEY) break;
    switch (pch) {
      case LINE_BEGIN_KEY :
      case HOME_KEY       : moveBufPtr(_readBuf); break;
      case LINE_END_KEY   :
      case END_KEY        : moveBufPtr(_readBufEnd); break;
      case BACK_SPACE_KEY : if( _readBuf != _readBufPtr ){
                              putchar( '\b' );
                              moveBufPtr( -- _readBufPtr );
                              deleteChar();
                            }
                            break;
      case DELETE_KEY     : deleteChar(); break;
      case NEWLINE_KEY    : addHistory();
                            cout << char(NEWLINE_KEY);
                            resetBufAndPrintPrompt(); break;
      case ARROW_UP_KEY   : moveToHistory(_historyIdx - 1); break;
      case ARROW_DOWN_KEY : moveToHistory(_historyIdx + 1); break;
      case ARROW_RIGHT_KEY: if( _readBufPtr != _readBufEnd ){
                              putchar( *_readBufPtr );
                              moveBufPtr( ++_readBufPtr );
                            }
                            break;
      case ARROW_LEFT_KEY : if( _readBufPtr != _readBuf ){
                              putchar( '\b' );
                              moveBufPtr( --_readBufPtr );
                            }
                            break;
      case PG_UP_KEY      : moveToHistory(_historyIdx - PG_OFFSET); break;
      case PG_DOWN_KEY    : moveToHistory(_historyIdx + PG_OFFSET); break;
      case TAB_KEY        :  {int _pos = 0;
                              char* _tmp = _readBuf;
                              while( _tmp != _readBufPtr )
                                _pos ++, _tmp ++;
                              _pos %= TAB_POSITION;
                              int _nd = TAB_POSITION - _pos;
                              insertChar( ' ' , _nd );}
                            break;
      case INSERT_KEY     : // not yet supported; fall through to UNDEFINE
      case UNDEFINED_KEY:   mybeep(); break;
      default:  // printable character
                            insertChar(char(pch)); break;
    }
#ifdef TA_KB_SETTING
    taTestOnly();
#endif
  }
}


// This function moves _readBufPtr to the "ptr" pointer
// It is used by left/right arrowkeys, home/end, etc.
//
// Suggested steps:
// 1. Make sure ptr is within [_readBuf, _readBufEnd].
//    If not, make a beep sound and return false. (DON'T MOVE)
// 2. Move the cursor to the left or right, depending on ptr
// 3. Update _readBufPtr accordingly. The content of the _readBuf[] will
//    not be changed
//
// [Note] This function can also be called by other member functions below
//        to move the _readBufPtr to proper position.
  bool
CmdParser::moveBufPtr(char* const ptr)
{
  // TODO...
  if( ptr < _readBuf ||
      ptr > _readBufEnd ){
    putchar( BEEP_CHAR );
    return false;
  }
// output
  while( _readBufPtr > ptr ){
    putchar( '\b' );
    _readBufPtr --;
  }
  while( _readBufPtr < ptr ){
    putchar( *_readBufPtr );
    _readBufPtr ++;
  }
  return true;
}


// [Notes]
// 1. Delete the char at _readBufPtr
// 2. mybeep() and return false if at _readBufEnd
// 3. Move the remaining string left for one character
// 4. The cursor should stay at the same position
// 5. Remember to update _readBufEnd accordingly.
// 6. Don't leave the tailing character.
// 7. Call "moveBufPtr(...)" if needed.
//
// For example,
//
// cmd> This is the command
//              ^                (^ is the cursor position)
//
// After calling deleteChar()---
//
// cmd> This is he command
//              ^
//

#define Clear _tPtr = _readBufPtr;\
              while( _tPtr != _readBufEnd ){\
                putchar( ' ' );\
                _tPtr ++;\
              }\
              while( _tPtr != _readBuf ){\
                printf( "\b \b" );\
                _tPtr --;\
              }
#define Show  _tPtr = _readBuf;\
              while( _tPtr != _readBufEnd ){\
                putchar( *_tPtr );\
                _tPtr ++;\
              }\
              while( _tPtr != _readBufPtr ){\
                putchar( '\b' );\
                _tPtr --;\
              }

  bool
CmdParser::deleteChar()
{
  // TODO...
  if( _readBufPtr == _readBufEnd ){
    putchar( BEEP_CHAR );
    return false;
  }
  char* _tPtr;
  Clear

  char* _tmpPtr = _readBufPtr;
  while( _tmpPtr != _readBufEnd ){
    char* _nxtPtr = _tmpPtr; _nxtPtr ++;
    *_tmpPtr = *_nxtPtr;
    _tmpPtr = _nxtPtr;
  }
  _readBufEnd --;
  *_readBufEnd = 0;

  Show

  return true;
}

// 1. Insert character 'ch' for "repeat" times at _readBufPtr
// 2. Move the remaining string right for "repeat" characters
// 3. The cursor should move right for "repeats" positions afterwards
// 4. Default value for "repeat" is 1. You should assert that (repeat >= 1).
//
// For example,
//
// cmd> This is the command
//              ^                (^ is the cursor position)
//
// After calling insertChar('k', 3) ---
//
// cmd> This is kkkthe command
//                 ^
//
  void
CmdParser::insertChar(char ch, int repeat )
{
  // TODO...
  char *_tPtr;
  Clear

  assert(repeat >= 1);
  char* _curBufEnd = _readBufEnd;
  for( int i = 0 ; i < repeat ; i ++ ) _readBufEnd ++;
  char* _curBufPtr = _readBufPtr;
  for( int i = 0 ; i < repeat ; i ++ ) _readBufPtr ++;
  if( _curBufEnd != _curBufPtr ){
    char* _tcurBufPtr = _curBufEnd;
    char* _tnxtBufPtr = _readBufEnd;
    do{
      _tcurBufPtr --; _tnxtBufPtr --;
      *_tnxtBufPtr = *_tcurBufPtr;
    }while( _tcurBufPtr != _curBufPtr );
  }
  for( int i = 0 ; i < repeat ; i ++, _curBufPtr ++ )
    *_curBufPtr = ch;
  *_readBufEnd = 0;

  Show
}

// 1. Delete the line that is currently shown on the screen
// 2. Reset _readBufPtr and _readBufEnd to _readBuf
// 3. Make sure *_readBufEnd = 0
//
// For example,
//
// cmd> This is the command
//              ^                (^ is the cursor position)
//
// After calling deleteLine() ---
//
// cmd>
//      ^
//
  void
CmdParser::deleteLine()
{
  // TODO...
  char* _tPtr;
  Clear

  _readBufPtr = _readBufEnd = _readBuf;
  *_readBufEnd = 0;
}


// This functions moves _historyIdx to index and display _history[index]
// on the screen.
//
// Need to consider:
// If moving up... (i.e. index < _historyIdx)
// 1. If already at top (i.e. _historyIdx == 0), beep and do nothing.
// 2. If at bottom, temporarily record _readBuf to history.
//    (Do not remove spaces, and set _tempCmdStored to "true")
// 3. If index < 0, let index = 0.
//
// If moving down... (i.e. index > _historyIdx)
// 1. If already at bottom, beep and do nothing
// 2. If index >= _history.size(), let index = _history.size() - 1.
//
// Assign _historyIdx to index at the end.
//
// [Note] index should not = _historyIdx
//
  void
CmdParser::moveToHistory(int index)
{
  // TODO...
  char* _tPtr;
  Clear

  if( _historyIdx == (int)_history.size() ){
    string _ts = "";
    char *tptr = _readBuf;
    while( tptr != _readBufEnd ){
      _ts += *tptr;
      tptr ++;
    }
    _history.push_back( _ts );
    _tempCmdStored = true;
  }
  if( index < _historyIdx ){
    if( index < 0 ) index = 0;
    if( _historyIdx == 0 ) putchar( BEEP_CHAR );
  }else if( index > _historyIdx ){
    if( index >= (int)_history.size() )
      index = (int)_history.size() - 1;
    if( _historyIdx == (int)_history.size() - 1 )
      putchar( BEEP_CHAR );
  }
  _historyIdx = index;
  _readBufPtr = _readBufEnd = _readBuf;
  for( int i = 0 ; i < (int)_history[ index ].length() ; i ++ ){
    *_readBufPtr = _history[ index ][ i ];
    _readBufPtr ++;
    _readBufEnd ++;
  }
  *_readBufEnd = 0;

  Show
}


// This function adds the string in _readBuf to the _history.
// The size of _history may or may not change. Depending on whether 
// there is a temp history string.
//
// 1. Remove ' ' at the beginning and end of _readBuf
// 2. If not a null string, add string to _history.
//    Be sure you are adding to the right entry of _history.
// 3. If it is a null string, don't add anything to _history.
// 4. Make sure to clean up "temp recorded string" (added earlier by up/pgUp,
//    and reset _tempCmdStored to false
// 5. Reset _historyIdx to _history.size() // for future insertion
//
  void
CmdParser::addHistory()
{
  // TODO...
  string _ts = "";
  char *tptr = _readBuf;
  while( tptr != _readBufEnd ){
    _ts += *tptr;
    tptr ++;
  }
  int spos = 0, tpos = _ts.length() - 1;
  while( spos < tpos && _ts[ spos ] == ' ' ) spos ++;
  while( tpos >= spos && _ts[ tpos ] == ' ' ) tpos --;
  string _nts = "";
  for( int i = spos ; i <= tpos ; i ++ ) _nts += _ts[ i ];
  if( _tempCmdStored ){
    _history.pop_back();
    _tempCmdStored = false;
  }
  if( _nts.length() > 0 )
    _history.push_back( _nts );
  _historyIdx = (int)_history.size();
  _readBufPtr = _readBufEnd = _readBuf;
  *_readBuf = 0;
}


// 1. Replace current line with _history[_historyIdx] on the screen
// 2. Set _readBufPtr and _readBufEnd to end of line
//
// [Note] Do not change _history.size().
//
  void
CmdParser::retrieveHistory()
{
  deleteLine();
  strcpy(_readBuf, _history[_historyIdx].c_str());
  cout << _readBuf;
  _readBufPtr = _readBufEnd = _readBuf + _history[_historyIdx].size();
}
