// eddy1021
#include <bits/stdc++.h>
using namespace std;
class Data{
private:
    int *_cols;
public:
    Data(){}
    Data( int _colNum ){
        _cols = new int[ _colNum ];
    }
    void init( int _colNum ){
        _cols = new int[ _colNum ];
    }
    const int operator[]( size_t i ) const{
        return _cols[ i ];
    }
    int& operator[]( size_t i ){
        return _cols[ i ];
    }
};
#define NONE 514514
int nextInt( ifstream& in ){
    char ctmp;
    int num = 0 , sgn = 1;
    bool gotInt = false;
    while( in.get( ctmp ) ){
        if( ctmp == ',' || (int)ctmp == 13 ) break;
        if( ctmp == '-' ) sgn = -1;
        if( ctmp >= '0' && ctmp <= '9' )
            num = num * 10 + ctmp - '0' , gotInt = true;
    }
    if( gotInt ) return num * sgn;
    return NONE;
}
void init( ifstream& in , vector<Data>& table , int rowNum , int colNum ){
    for( int i = 0 ; i < rowNum ; i ++ ){
        Data tData( colNum );
        for( int j = 0 ; j < colNum ; j ++ )
            tData[ j ] = nextInt( in );
        table.push_back( tData );
    }
}
int parse( string& nxtInt ){
    if( strcmp( nxtInt.c_str() , "-" ) == 0 ) return NONE;
    int num = 0, sgn = 1, len = nxtInt.length();
    for( int i = 0 ; i < len ; i ++ )
        if( nxtInt[ i ] == '-' ) sgn = -1;
        else if( nxtInt[ i ] >= '0' && nxtInt[ i ] <= '9' )
            num = num * 10 + nxtInt[ i ] - '0';
    return num * sgn;
}
struct SortData{
    bool operator()( const Data& d1 , const Data& d2 ){
        for( size_t i = 0 ; i < _sortOrder.size() ; i ++ )
            if( d1[ _sortOrder[ i ] ] != d2[ _sortOrder[ i ] ] )
                return d1[ _sortOrder[ i ] ] < d2[ _sortOrder[ i ] ];
        return false;
    }
    void pushOrder(size_t i){
        _sortOrder.push_back( i );
    }
    vector<size_t> _sortOrder;
};
void solve( vector<Data>& table , int rowNum , int colNum ){
    string cmd;
    while( cin >> cmd ){
        if( strcmp( cmd.c_str() , "PRINT" ) == 0 ){
            for( int i = 0 ; i < rowNum ; i ++ ){
                for( int j = 0 ; j < colNum ; j ++ )
                    if( table[ i ][ j ] != NONE )
                        cout << right << setw( 4 ) << table[ i ][ j ];
                    else cout << "    ";
                cout << endl;
            }
        }
        if( strcmp( cmd.c_str() , "ADD" ) == 0 ){
            Data tData( colNum );
            string nxtInt;
            for( int j = 0 ; j < colNum ; j ++ ){
                cin >> nxtInt;
                tData[ j ] = parse( nxtInt );
            }
            table.push_back( tData );
            rowNum ++;
        }
        if( strcmp( cmd.c_str() , "SUM" ) == 0 ){
            int qcol; cin >> qcol;
            int ans = 0;
            for( int i = 0 ; i < rowNum ; i ++ )
                if( table[ i ][ qcol ] != NONE )
                    ans += table[ i ][ qcol ];
            cout << "The summation of data in column #" << qcol;
            cout << " is " << ans << ".\n";
        }
        if( strcmp( cmd.c_str() , "AVE" ) == 0 ){
            int qcol; cin >> qcol;
            double ans = 0;
            int cnt = 0;
            for( int i = 0 ; i < rowNum ; i ++ )
                if( table[ i ][ qcol ] != NONE )
                    ans += table[ i ][ qcol ] , cnt ++;
            if( cnt ) ans /= (double)cnt;
            cout << "The average of data in column #" << qcol;
            cout << " is " << fixed << setprecision( 3 ) << ans << ".\n";
        }
#define INF 100000
        if( strcmp( cmd.c_str() , "MAX" ) == 0 ){
            int qcol; cin >> qcol;
            int ans = -INF;
            for( int i = 0 ; i < rowNum ; i ++ )
                if( table[ i ][ qcol ] != NONE )
                    ans = max( ans , table[ i ][ qcol ] );
            cout << "The maximum of data in column #" << qcol;
            cout << " is " << ans << ".\n";
        }
        if( strcmp( cmd.c_str() , "MIN" ) == 0 ){
            int qcol; cin >> qcol;
            int ans = INF;
            for( int i = 0 ; i < rowNum ; i ++ )
                if( table[ i ][ qcol ] != NONE )
                    ans = min( ans , table[ i ][ qcol ] );
            cout << "The minimum of data in column #" << qcol;
            cout << " is " << ans << ".\n";
        }
        if( strcmp( cmd.c_str() , "COUNT" ) == 0 ){
            int qcol; cin >> qcol;
            set<int> S;
            for( int i = 0 ; i < rowNum ; i ++ )
                if( table[ i ][ qcol ] != NONE )
                    S.insert( table[ i ][ qcol ] );
            cout << "The distinct count of data in column #" << qcol;
            cout << " is " << (int)S.size() << ".\n";
        }
        if( strcmp( cmd.c_str() , "SORT" ) == 0 ){
            string order;
            SortData tSortData;
            getline( cin , order );
            int len = order.length();
            int num = 0; bool got = false;
            for( int i = 0 ; i <= len ; i ++ )
                if( i == len || order[ i ] < '0' || order[ i ] > '9' ){
                    if( got ) tSortData.pushOrder( (size_t)num );
                    got = false; num = 0;
                }else if( order[ i ] >= '0' && order[ i ] <= '9' ){
                    got = true;
                    num = num * 10 + order[ i ] - '0';
                }
            sort( table.begin() , table.end() , tSortData );
        }
    }
}
int main(){
    string fileName;
    int rowNum, colNum;
    ifstream in;
    vector<Data> table;
    cout << "Please enter the file name: ";
    cin >> fileName;
    cout << "Please enter the number of rows and columns: ";
    cin >> rowNum >> colNum;
    in.open( fileName.c_str() );
    if( in.is_open() ){
        cout << "File \"" << fileName << "\" was read in successfully." << endl;
        init( in , table , rowNum , colNum );
        solve( table , rowNum , colNum );
    }else{
        cout << "File \"" << fileName << "\" was NOT read in" << endl;
        return 0;
    }
}
