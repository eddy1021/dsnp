/****************************************************************************
  FileName     [ array.h ]
  PackageName  [ util ]
  Synopsis     [ Define dynamic array package ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2005-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef ARRAY_H
#define ARRAY_H

#include <cassert>
#include <algorithm>

using namespace std;

// NO need to implement class ArrayNode
//
template <class T>
class Array
{
 public:
  Array() : _data(0), _size(0), _capacity(0) {}
  ~Array() { delete []_data; }

  // DO NOT add any more data member or function for class iterator
  class iterator
  {
    friend class Array;

   public:
    iterator(T* n= 0): _node(n) {}
    iterator(const iterator& i): _node(i._node) {}
    ~iterator() {} // Should NOT delete _node

    // TODO: implement these overloaded operators
    const T& operator * () const { return (*_node); }
    T& operator * () { return (*_node); }
    iterator& operator ++ () { ++ _node; return(*this); }
    iterator operator ++ (int) {
      iterator it = (*this);
      ++ _node;
      return it; }
    iterator& operator -- () { -- _node; return (*this); }
    iterator operator -- (int) {
      iterator it = (*this);
      -- _node;
      return it; }
    iterator operator + (int i) const {
      iterator it = (*this);
      it._node += i;
      return it;
    }
    iterator& operator += (int i) {
      _node += i;
      return (*this);
    }
    iterator& operator = (const iterator& i) { 
      _node = i._node;
      return (*this);
    }
    bool operator != (const iterator& i) const { return _node != i._node; }
    bool operator == (const iterator& i) const { return _node == i._node; }

   private:
    T*    _node;
  };

  // TODO: implement these functions
  iterator begin() const {
    iterator it( _data );
    return it;
  }
  iterator end() const {
    iterator it( _data );
    it += _size;
    return it;
  }
  bool empty() const { return size() == 0; }
  size_t size() const { return _size; }

  T& operator [] (size_t i) { return _data[i]; }
  const T& operator [] (size_t i) const { return _data[i]; }

  void push_back(const T& x) {
    if( _size == _capacity ){
      size_t extra = _capacity;
      if( extra == 0 ) extra = 1;
      _capacity += extra;
      T* _tdata = new T[ _capacity ];
      for( size_t _ = 0 ; _ < _size ; _ ++ )
        _tdata[ _ ] = _data[ _ ];
      delete [] _data;
      _data = _tdata;
    }
    _data[ _size ++ ] = x;
  }
  void pop_front() {
    if( _size == 0 ) return;
    erase( 0 );
  }
  void pop_back() {
    if( _size == 0 ) return;
    _size --;
  }
  bool erase(iterator pos) {
    iterator it( _data );
    for( int i = 0 ; i < (int)_size ; i ++ , it ++ )
      if( it == pos )
        return erase( i );
    return false;
  }
  bool erase(const T& x) {
    for( int i = 0 ; i < (int)_size ; i ++ )
      if( _data[ i ] == x )
        return erase( i );
    return false;
  }
  void clear() {
    _size = 0;
  }

  // This is done. DO NOT change this one.
  void sort() const { if (!empty()) ::sort(_data, _data+_size); }

  // Nice to have, but not required in this homework...
  // void reserve(size_t n) { ... }
  // void resize(size_t n) { ... }

 private:
  T*           _data;
  size_t       _size;       // number of valid elements
  size_t       _capacity;   // max number of elements

  // [OPTIONAL TODO] Helper functions; called by public member functions
  bool erase( int idx ) {
    if( idx < 0 || idx >= (int)_size ) return false;
    for( int i = idx ; i < (int)_size - 1 ; i ++ )
      _data[ i ] = _data[ i + 1 ];
    pop_back();
    return true;
  }
};

#endif // ARRAY_H
