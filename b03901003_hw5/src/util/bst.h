/****************************************************************************
  FileName     [ bst.h ]
  PackageName  [ util ]
  Synopsis     [ Define binary search tree package ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2005-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef BST_H
#define BST_H

#include <cassert>

using namespace std;

template <class T> class BSTree;

// BSTreeNode is supposed to be a private class. User don't need to see it.
// Only BSTree and BSTree::iterator can access it.
//
// DO NOT add any public data member or function to this class!!
//
template <class T>
class BSTreeNode{
  // TODO: design your own class!!
public:
  friend class BSTree<T>;
  friend class BSTree<T>::iterator;
  BSTreeNode( int tpri ): _pri( tpri ){
    _sz = 1; _left = _right = _par = NULL;
  }
  BSTreeNode( const T& tdata,
              BSTreeNode<T>* tlft,
              BSTreeNode<T>* trgt ):
              _data( tdata ), _left( tlft ), _right( trgt ){
    _sz = 1;
    _pri = myRand();
    _par = NULL;
  }
  BSTreeNode( const BSTreeNode<T>& x ){
    _pri = x._pri;
    _sz = x._sz;
    _left = x._left;
    _right = x._right;
    _par = x._par;
    _data = x._data;
  }
  int myRand(){
typedef long long ll;
    myRnd = ( (ll)myRnd * (ll)base + (ll)delt ) % (ll)myMod;
    return myRnd;
  }
  static int myRnd;
  static int base;
  static int delt;
  static int myMod;
  int _pri, _sz;
  T _data;
  BSTreeNode<T>* _left;
  BSTreeNode<T>* _right;
  BSTreeNode<T>* _par;
};
template <class T>
int BSTreeNode<T>::myRnd = 26683;
template <class T>
int BSTreeNode<T>::base = 100049;
template <class T>
int BSTreeNode<T>::delt = 102101;
template <class T>
int BSTreeNode<T>::myMod = 1000000007;

template <class T>
class BSTree{
  // TODO: design your own class!!
public:
#define X 2100000000
#define BEGIN (-X+1)
#define END (-X)
  BSTree(){
    BSTreeNode<T>* _pBegin = new BSTreeNode<T>( BEGIN );
    BSTreeNode<T>* _End = new BSTreeNode<T>( END );
    _root = merge( _pBegin , _End );
    _size = 2;
  }
  ~BSTree(){ clear(); delete _root; }
  class iterator {
    friend class BSTree;
   public:
    iterator(BSTreeNode<T>* n = 0): _node(n) {}
    iterator(const iterator& i) : _node(i._node) {}
    ~iterator() {} // Should NOT delete _node

    const T& operator * () const { return _node->_data; }
    T& operator * () { return _node->_data; }
    BSTreeNode<T>* Next(){
      if( _node->_right ){
        BSTreeNode<T>* _ptr = _node->_right;
        while( _ptr->_left ) _ptr = _ptr->_left;
        return _ptr;
      }
      BSTreeNode<T>* _prev = _node;
      BSTreeNode<T>* _ptr = _node->_par;
      while( _ptr ){
        if( _ptr->_left == _prev )
          return _ptr;
        BSTreeNode<T>* _nxt = _ptr->_par;
        _prev = _ptr;
        _ptr = _nxt;
      }
      return NULL;
    }
    BSTreeNode<T>* Prev(){
      if( _node->_left ){
        BSTreeNode<T>* _ptr = _node->_left;
        while( _ptr->_right ) _ptr = _ptr->_right;
        return _ptr;
      }
      BSTreeNode<T>* _prev = _node;
      BSTreeNode<T>* _ptr = _node->_par;
      while( _ptr ){
        if( _ptr->_right == _prev )
          return _ptr;
        BSTreeNode<T>* _nxt = _ptr->_par;
        _prev = _ptr;
        _ptr = _nxt;
      }
      return NULL;
    }
    iterator& operator ++ () {
      _node = Next();
      return (*this);
    }
    iterator operator ++ (int) {
      iterator it( _node );
      _node = Next();
      return it;
    }
    iterator& operator -- () {
      _node = Prev();
      return (*this);
    }
    iterator operator -- (int) {
      iterator it( _node );
      _node = Prev();
      return it;
    }
    iterator& operator = (const iterator& i) { _node = i._node; return *(this); }
    bool operator != (const iterator& i) const { return _node != i._node; }
    bool operator == (const iterator& i) const { return _node == i._node; }

   private:
    BSTreeNode<T>* _node;
  };
  int Size( BSTreeNode<T>* tn ){
    return tn ? tn->_sz : 0;
  }
  void pull( BSTreeNode<T>*& tn ){
    tn->_sz = 1 + Size( tn->_left ) + Size( tn->_right );
  }
  BSTreeNode<T>* merge( BSTreeNode<T>* t1 , BSTreeNode<T>* t2 ){
    if( !t1 || !t2 ) return t1 ? t1 : t2;
    if( t1->_pri > t2->_pri ){
      t1->_right = merge( t1->_right , t2 );
      if( t1->_right ) t1->_right->_par = t1;
      pull( t1 );
      return t1;
    }else{
      t2->_left = merge( t1 , t2->_left );
      if( t2->_left ) t2->_left->_par = t2;
      pull( t2 );
      return t2;
    }
  }
  void split( BSTreeNode<T>* t , int k , BSTreeNode<T>*& t1 , BSTreeNode<T>*& t2 ){
    if( !t ){ t1 = t2 = NULL; return; }
    if( Size( t->_left ) + 1 <= k ){
      t1 = t;
      split( t->_right , k - Size( t->_left ) - 1 , t1->_right , t2 );
      if( t1->_right ) t1->_right->_par = t1;
      pull( t1 );
    }else{
      t2 = t;
      split( t->_left , k , t1 , t2->_left );
      if( t2->_left ) t2->_left->_par = t2;
      pull( t2 );
    }
  }
  int lowerBound( T x ){
    if( size() == 0 ) return 1;
    BSTreeNode<T>* _ptr( _root );
    int lsum = 0;
    while( _ptr ){
      if( _ptr->_pri == END ){
        _ptr = _ptr->_left;
      }else if( _ptr->_pri == BEGIN ){
        _ptr = _ptr->_right, lsum ++;
      }else if( _ptr->_data < x ){
        lsum += Size( _ptr->_left ) + 1;
        _ptr = _ptr->_right;
      }else if( _ptr->_data == x ){
        lsum += Size( _ptr->_left );
        return lsum;
      }else{
        _ptr = _ptr->_left;
      }
    }
    assert( lsum > 0 );
    return lsum;
  }
  int find( T x ){
    if( size() == 0 ) return -1;
    BSTreeNode<T>* _ptr( _root );
    int lsum = 0;
    while( _ptr ){
      if( _ptr->_pri == END )
        _ptr = _ptr->_left;
      else if( _ptr->_pri == BEGIN )
        _ptr = _ptr->_right, lsum ++;
      else if( _ptr->_data < x ){
        lsum += Size( _ptr->_left ) + 1;
        _ptr = _ptr->_right;
      }else if( _ptr->_data == x )
        return lsum + Size( _ptr->_left );
      else _ptr = _ptr->_left;
    }
    return -1;
  }
  iterator begin() const {
    iterator it( _root );
    if( size() == 0 ) return end();
    while( it._node->_left ){
      iterator it2( it._node->_left );
      it = it2;
    }
    ++ it;
    return it; 
  }
  iterator end() const {
    iterator it( _root );
    while( it._node->_right ){
      iterator it2( it._node->_right );
      it = it2;
    }
    return it;
  }
  bool empty() const { return size() == 0; }
  size_t size() const { return _size - 2; }
  void insert(const T& x) {
    BSTreeNode<T>* newNode = new BSTreeNode<T>( x , NULL , NULL );
    int pos = lowerBound( x );
    BSTreeNode<T>* tmpl;
    BSTreeNode<T>* tmpr;
    split( _root , pos , tmpl , tmpr );
    _root = merge( tmpl , newNode );
    _root = merge( _root , tmpr );
    begin();
    _size ++;
  }
  void pop_front(){
    if( size() == 0 ) return;
    BSTreeNode<T>* dummy;
    BSTreeNode<T>* saveBegin;
    split( _root , 1 , saveBegin , _root );
    split( _root , 1 , dummy , _root );
    _root = merge( saveBegin , _root );
    delete dummy;
    _size --;
  }
  void pop_back(){
    if( size() == 0 ) return;
    BSTreeNode<T>* saveEnd;
    split( _root , size() + 1 , _root , saveEnd );
    BSTreeNode<T>* dummy;
    split( _root , size() , _root , dummy );
    _root = merge( _root , saveEnd );
    delete dummy;
    _size --;
  }
  void print( BSTreeNode<T>* _now ){
    if( !_now ) return;
    print( _now->_left );
    cerr << "[" << printPtr ++ << "] = ";
    cerr << _now->_data << "   ";
    if( printPtr % 4 == 0 ) cout << endl;
    print( _now->_right );
  }
  void print(){
    cout << "=== ADT (bst) ===" << endl;
    printPtr = 0;
    print( _root );
    if( printPtr % 4 != 0 ) cout << endl;
  }

  // return false if nothing to erase
  bool erase(iterator pos) {
    return erase( pos._node->_data );
  }
  bool erase(const T& x) {
    int ipos = find( x );
    if( ipos == -1 ) return false;
    BSTreeNode<T>* tl;
    BSTreeNode<T>* tr;
    split( _root , ipos , _root , tr );
    split( tr , 1 , tl , tr );
    delete tl;
    _root = merge( _root , tr );
    _size --;
    return true;
  }
  void clear( BSTreeNode<T>* _node ){
    if( _node->_left ) clear( _node->_left );
    if( _node->_right ) clear( _node->_right );
    delete _node;
  }
  void clear() {
    clear( _root );
    BSTreeNode<T>* _End = new BSTreeNode<T>( END );
    BSTreeNode<T>* _pBegin = new BSTreeNode<T>( BEGIN );
    _root = merge( _pBegin , _End );
    _size = 2;
  }
  void sort() const { /*do nothing*/ }

 private:
  BSTreeNode<T>* _root;
  int _size, printPtr;
};

#endif // BST_H
