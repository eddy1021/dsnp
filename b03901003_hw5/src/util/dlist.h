/****************************************************************************
  FileName     [ dlist.h ]
  PackageName  [ util ]
  Synopsis     [ Define doubly linked list package ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2005-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef DLIST_H
#define DLIST_H

#include <cassert>

template <class T> class DList;

// DListNode is supposed to be a private class. User don't need to see it.
// Only DList and DList::iterator can access it.
//
// DO NOT add any public data member or function to this class!!
//
template <class T>
class DListNode
{
  friend class DList<T>;
  friend class DList<T>::iterator;

  DListNode(const T& d, DListNode<T>* p = 0, DListNode<T>* n = 0):
    _data(d), _prev(p), _next(n) {}

  T              _data;
  DListNode<T>*  _prev;
  DListNode<T>*  _next;
};


template <class T>
class DList
{
 public:
  DList() {
    _head = new DListNode<T>(T());
    _head->_prev = _head->_next = _head; // _head is a dummy node
  }
  ~DList() { clear(); delete _head; }

  // DO NOT add any more data member or function for class iterator
  class iterator
  {
    friend class DList;

   public:
    iterator(DListNode<T>* n= 0): _node(n) {}
    iterator(const iterator& i) : _node(i._node) {}
    ~iterator() {} // Should NOT delete _node

    // TODO: implement these overloaded operators
    const T& operator * () const { return _node->_data; }
    T& operator * () { return _node->_data; }
    iterator& operator ++ () { _node = _node->_next; return *(this); }
    iterator operator ++ (int) {
      iterator it( *this );
      _node = _node->_next;
      return it;
    }
    iterator& operator -- () { _node = _node->_prev; return *(this); }
    iterator operator -- (int) {
      iterator it( *this );
      _node = _node->_prev;
      return it;
    }
    iterator& operator = (const iterator& i) { _node = i._node; return *(this); }

    bool operator != (const iterator& i) const { return !( *this == i ); }
    bool operator == (const iterator& i) const { return _node == i._node; }

   private:
    DListNode<T>* _node;
  };

  // TODO: implement these functions
  iterator begin() const { return _head; } 
  iterator end() const { return _head->_prev; }
  bool empty() const { return size()==0; }
  size_t size() const {
    DListNode<T>*  _ptr = _head;
    int cnt = 0;
    while( _ptr != _head->_prev ){
      cnt ++;
      _ptr = _ptr->_next;
    }
    return cnt;
  }
  void push_back(const T& x) {
    DListNode<T>* _tmp = new DListNode<T>( x , _head->_prev->_prev , _head->_prev );
    _head->_prev->_prev->_next = _tmp;
    _head->_prev->_prev = _tmp;
  }
  void pop_front(){
    if( size() == 0 ) return;
    _head->_next->_prev = _head->_prev;
    _head->_prev->_next = _head->_next;
    DListNode<T>*  _tmp = _head;
    _head = _head->_next;
    delete _tmp;
  }
  void pop_back() {
    if( size() == 0 ) return;
    DListNode<T>* _tmp = _head->_prev->_prev;
    _tmp->_prev->_next = _tmp->_next;
    _tmp->_next->_prev = _tmp->_prev;
    delete _tmp;
  }

  // return false if nothing to erase
  bool erase(iterator pos) {
    iterator it( _head );
    iterator St( _head );
    iterator End( _head->_prev );
    while( it != End ){
      if( it == pos ){
        it._node->_prev->_next = it._node->_next;
        it._node->_next->_prev = it._node->_prev;
        if( it == St ) _head = it._node->_next;
        delete it._node;
        return true;
      }
      it ++;
    }
    return false;
  }
  bool erase(const T& x) {
    DListNode<T>* _ptr( _head );
    while( _ptr != _head->_prev ){
      if( _ptr->_data == x ){
        _ptr->_prev->_next = _ptr->_next;
        _ptr->_next->_prev = _ptr->_prev;
        if( _ptr == _head ) _head = _ptr->_next;
        delete _ptr;
        return true;
      }
      _ptr = _ptr->_next;
    }
    return false;
  }
  void clear() {
    DListNode<T>*  _ptr( _head );
    while( _ptr != _head->_prev ){
      iterator it( _ptr );
      _ptr = _ptr->_next;
      erase( it );
    }
  }  // delete all nodes except for the dummy node

  void sort() const {
    size_t _sz = size();
    if( _sz == 0 ) return;
    iterator End( _head->_prev );
    T* datas = new T[ _sz ];
    T* tmp = new T[ _sz ];
    int _ptr = 0;
    for( iterator it( _head ) ; it != End ; it ++ )
      datas[ _ptr ++ ] = it._node->_data;
    MergeSort( 0 , _ptr - 1 , datas , tmp );
    _ptr = 0;
    for( iterator it( _head ) ; it != End ; it ++ )
      it._node->_data = datas[ _ptr ++ ];
    delete [] datas;
    delete [] tmp;
  }

 private:
  DListNode<T>*  _head;  // = dummy node if list is empty

  // [OPTIONAL TODO] helper functions; called by public member functions
  void MergeSort( int l , int r , T*& arr , T*& tmp ) const{
    if( l >= r ) return;
    int mid = ( l + r ) >> 1;
    MergeSort( l , mid , arr , tmp );
    MergeSort( mid + 1 , r , arr , tmp );
    int lptr = l, rptr = mid + 1 , tptr = l;
    while( lptr <= mid && rptr <= r ){
      if( arr[ lptr ] < arr[ rptr ] ){
        tmp[ tptr ++ ] = arr[ lptr ]; lptr ++;
      }else{
        tmp[ tptr ++ ] = arr[ rptr ]; rptr ++;
      }
    }
    while( lptr <= mid ) tmp[ tptr ++ ] = arr[ lptr ], lptr ++;
    while( rptr <= r ) tmp[ tptr ++ ] = arr[ rptr ], rptr ++;
    for( int i = l ; i <= r ; i ++ )
      arr[ i ] = tmp[ i ];
  }
};

#endif // DLIST_H
