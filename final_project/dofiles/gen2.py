fileName = [
  "C1355.aag",
  "C17.aag",
  "C1908.aag",
  "C3540.aag",
  "C432.aag",
  "C432_r.aag",
  "C499.aag",
  "C499_r.aag",
  "C5315.aag",
  "C6288.aag",
  "C7552.aag",
  "C880.aag" ]

i = 33
for x in fileName:
  i += 1
  f = open( "do" + str( i ) , "w" )
  f.write( "cirr tests.fraig/ISCAS85/" + x + "\n" )
  f.write( "cirp -S\n" )
  f.write( "cirp -N\n" )
  f.write( "cirp -PI\n" )
  f.write( "cirp -PO\n" )
  f.write( "cirp -FL\n" )
  f.write( "cirw\n" )
  f.write( "q -f\n" )
  f.close()
