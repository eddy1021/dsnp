/****************************************************************************
  FileName     [ cirGate.h ]
  PackageName  [ cir ]
  Synopsis     [ Define basic gate data structures ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef CIR_GATE_H
#define CIR_GATE_H

#include <string>
#include <vector>
#include <iostream>
#include "cirDef.h"
#include "sat.h"

using namespace std;

// TODO: Feel free to define your own classes, variables, or functions.

class CirGate;

//------------------------------------------------------------------------
//   Define classes
//------------------------------------------------------------------------
const static string gateTypeName[5] ={ "UNDEF","PI","PO","AIG","CONST" };
class CirGate
{
 public:
  CirGate() { zflag = indeg = stap2 = stap = gateType = 0;
              swept = reach = mark = usd = false; symbol = ""; }
  CirGate( int _id , int _lineNo , int type , string _sym = "" ):
    id(_id), lineNo( _lineNo ), gateType( type ), symbol( _sym ){
      swept = reach = mark = usd = false; symbol = "";
      zflag = stap2 = stap = indeg = 0;
    }
  ~CirGate() {}

  // my methods
  void setSymbol( string _sym ){ symbol = _sym; }
  string getSymbol() const{ return symbol; }
  unsigned getId() const { return id; }
  void addIn( CirGate* tGate , int inId , int tinv = 0 ){
    fanIn.push_back( tGate );
    inv.push_back( tinv );
    idIn.push_back( inId );
  }
  void addOut( CirGate* tGate , int outId , int tinv = 0 ){
    fanOut.push_back( tGate );
    onv.push_back( tinv );
    idOut.push_back( outId );
  }
  void MyreportFanin(int level, string pre,int _inv);
  void MyreportFanout(int level, string pre,int _inv);
  void iClean();
  void oClean();

  // Basic access methods
  string getTypeStr() const { return gateTypeName[ gateType ]; }
  unsigned getLineNo() const { return lineNo; }
  unsigned getType() const { return gateType; }
  virtual bool isAig() const { return gateType == 3; }

  // Printing functions
  virtual void printGate() const {}
  void reportGate() const;
  void reportFanin(int level);
  void reportFanout(int level);

typedef vector<CirGate*> GateList;
typedef unsigned long long ULL;
  GateList fanIn, fanOut;
  vector<int> idIn, idOut;
  vector<int> inv, onv;
  ULL result , magicResult;
  int indeg , stap , stap2 , topoId , piId;
  bool mark, usd, reach , swept;
  Var varId;
  char zflag;
  vector<int> fecg;
 private:
  unsigned id;
  unsigned lineNo, gateType;
  string symbol;
  void printBlock() const{
    cout << "==================================================" << endl;
  }
};

#endif // CIR_GATE_H
