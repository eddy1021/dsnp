/****************************************************************************
  FileName     [ cirMgr.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define cir manager functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstdio>
#include <ctype.h>
#include <cassert>
#include <cstring>
#include <sstream>
#include <queue>
#include "cirMgr.h"
#include "cirGate.h"
#include "util.h"

using namespace std;

// TODO: Implement memeber functions for class CirMgr

/*******************************/
/*   Global variable and enum  */
/*******************************/
CirMgr* cirMgr = 0;

enum CirParseError {
  EXTRA_SPACE,
  MISSING_SPACE,
  ILLEGAL_WSPACE,
  ILLEGAL_NUM,
  ILLEGAL_IDENTIFIER,
  ILLEGAL_SYMBOL_TYPE,
  ILLEGAL_SYMBOL_NAME,
  MISSING_NUM,
  MISSING_IDENTIFIER,
  MISSING_NEWLINE,
  MISSING_DEF,
  CANNOT_INVERTED,
  MAX_LIT_ID,
  REDEF_GATE,
  REDEF_SYMBOLIC_NAME,
  REDEF_CONST,
  NUM_TOO_SMALL,
  NUM_TOO_BIG,

  DUMMY_END
};

/**************************************/
/*   Static varaibles and functions   */
/**************************************/
static unsigned lineNo = 0;  // in printint, lineNo needs to ++
static unsigned colNo  = 0;  // in printing, colNo needs to ++
static string errMsg;
static int errInt;
static CirGate *errGate;

  static bool
parseError(CirParseError err)
{
  switch (err) {
    case EXTRA_SPACE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Extra space character is detected!!" << endl;
      break;
    case MISSING_SPACE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Missing space character!!" << endl;
      break;
    case ILLEGAL_WSPACE: // for non-space white space character
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Illegal white space char(" << errInt
        << ") is detected!!" << endl;
      break;
    case ILLEGAL_NUM:
      cerr << "[ERROR] Line " << lineNo+1 << ": Illegal "
        << errMsg << "!!" << endl;
      break;
    case ILLEGAL_IDENTIFIER:
      cerr << "[ERROR] Line " << lineNo+1 << ": Illegal identifier \""
        << errMsg << "\"!!" << endl;
      break;
    case ILLEGAL_SYMBOL_TYPE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Illegal symbol type (" << errMsg << ")!!" << endl;
      break;
    case ILLEGAL_SYMBOL_NAME:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Symbolic name contains un-printable char(" << errInt
        << ")!!" << endl;
      break;
    case MISSING_NUM:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Missing " << errMsg << "!!" << endl;
      break;
    case MISSING_IDENTIFIER:
      cerr << "[ERROR] Line " << lineNo+1 << ": Missing \""
        << errMsg << "\"!!" << endl;
      break;
    case MISSING_NEWLINE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": A new line is expected here!!" << endl;
      break;
    case MISSING_DEF:
      cerr << "[ERROR] Line " << lineNo+1 << ": Missing " << errMsg
        << " definition!!" << endl;
      break;
    case CANNOT_INVERTED:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": " << errMsg << " " << errInt << "(" << errInt/2
        << ") cannot be inverted!!" << endl;
      break;
    case MAX_LIT_ID:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Literal \"" << errInt << "\" exceeds maximum valid ID!!"
        << endl;
      break;
    case REDEF_GATE:
      cerr << "[ERROR] Line " << lineNo+1 << ": Literal \"" << errInt
        << "\" is redefined, previously defined as "
        << errGate->getTypeStr() << " in line " << errGate->getLineNo()
        << "!!" << endl;
      break;
    case REDEF_SYMBOLIC_NAME:
      cerr << "[ERROR] Line " << lineNo+1 << ": Symbolic name for \""
        << errMsg << errInt << "\" is redefined!!" << endl;
      break;
    case REDEF_CONST:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Cannot redefine const (" << errInt << ")!!" << endl;
      break;
    case NUM_TOO_SMALL:
      cerr << "[ERROR] Line " << lineNo+1 << ": " << errMsg
        << " is too small (" << errInt << ")!!" << endl;
      break;
    case NUM_TOO_BIG:
      cerr << "[ERROR] Line " << lineNo+1 << ": " << errMsg
        << " is too big (" << errInt << ")!!" << endl;
      break;
    default: break;
  }
  return false;
}

/**************************************************************/
/*   class CirMgr member functions for circuit construction   */
/**************************************************************/
bool
CirMgr::readCircuit(const string& fileName){
  if( false ) parseError( NUM_TOO_BIG );
  ifstream in( fileName.data() , ifstream::in );
  if( !in.is_open() ) return false;
  nM = nI = nL = nO = nA = usdA = 0;
  iList.clear(); oList.clear();
  aList.clear(); mList.clear();
  floating.clear(); notusd.clear();
  string inputBuf , dump;
  bool header = false;
  int got = 0, lno = 0, po = 0;
  vector<int> st , i1 , i2;
  while( getline( in , inputBuf ) ){
    lineNo = lno; colNo = 0;
    lno ++; 
    int inputLen = inputBuf.length();
    if( inputLen > 0 && inputBuf[ 0 ] == 'c' ) break;
    string dump;
    if( !header ){
      header = true;
      stringstream ss; ss << inputBuf;
      ss >> dump >> nM >> nI >> nL >> nO >> nA;
      mList.resize( nM + 1 );
      mList[ 0 ] = new CirGate( 0 , 0 , 4 );
    }else if( got < nI ){
      stringstream ss; ss << inputBuf;
      int tid; ss >> tid;
      CirGate* tGate = new CirGate( tid / 2 , lno , 1 );
      iList.push_back( tGate );
      mList[ tid / 2 ] = tGate;
      got ++;
    }else if( got < nI + nL ){
      stringstream ss; ss << inputBuf;
      int tid; ss >> tid;
      //CirGate* tGate = new CirGate( tid , lno );
      //lList.push_back( tGate );
      got ++;
    }else if( got < nI + nL + nO ){
      stringstream ss; ss << inputBuf;
      int tid; ss >> tid; po ++;
      int nid = nM + po;
      CirGate* tGate = new CirGate( nid , lno , 2 );
      oList.push_back( tGate );
      mList.push_back( tGate );
      st.push_back( tid );
      got ++;
    }else if( got < nI + nL + nO + nA ){
      stringstream ss; ss << inputBuf;
      int tid , in1 , in2;
      ss >> tid >> in1 >> in2;
      CirGate* tGate = new CirGate( tid / 2 , lno , 3 );
      aList.push_back( tGate );
      mList[ tid / 2 ] = tGate;
      i1.push_back( in1 );
      i2.push_back( in2 );
      got ++;
    }else{
      stringstream ss; ss << inputBuf;
      string sid , sym;
      ss >> sid >> sym;
      int nid = atoi( sid.substr( 1 ).data() );
      if( sid[ 0 ] == 'o' ) nid += nM;
      if( mList[ nid + 1 ] )
        mList[ nid + 1 ]->setSymbol( sym );
    }
  }
  for( size_t i = 0 ; i < iList.size() ; i ++ )
    iList[ i ]->piId = i;

  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( !mList[ i ] )
      mList[ i ] = new CirGate( i , 0 , 0 );

  for( size_t i = 0 ; i < oList.size() ; i ++ )
    buildEdge( mList[ st[ i ] / 2 ] , oList[ i ] , st[ i ] & 1 , st[ i ] / 2 , oList[ i ]->getId() );

  for( size_t i = 0 ; i < aList.size() ; i ++ ){
    buildEdge( mList[ i1[ i ] / 2 ] , aList[ i ] , i1[ i ] & 1 , i1[ i ] / 2 , aList[ i ]->getId() );
    buildEdge( mList[ i2[ i ] / 2 ] , aList[ i ] , i2[ i ] & 1 , i2[ i ] / 2 , aList[ i ]->getId() );
  }

  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] )
      mList[ i ]->mark = false;

  for( size_t i = 0 ; i < oList.size() ; i ++ )
    go( oList[ i ] );

  usdA = 0;
  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( aList[ i ]->mark ){
      usdA ++;
      aList[ i ]->usd = true;
    }
  
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] )
      mList[ i ]->mark = false;

  markReachableGates();
  TopoSort();
  
  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( aList[ i ] ){
      if( aList[ i ]->fanIn[ 0 ]->topoId >
          aList[ i ]->fanIn[ 1 ]->topoId ){
        swap( aList[ i ]->fanIn[ 0 ] , aList[ i ]->fanIn[ 1 ] );
        swap( aList[ i ]->idIn[ 0 ] , aList[ i ]->idIn[ 1 ] );
        swap( aList[ i ]->inv[ 0 ] , aList[ i ]->inv[ 1 ] );
      }
    }

  return true;
}

void CirMgr::go( CirGate* tGate ){
  if( !tGate ) return;
  if( tGate->mark ) return;
  tGate->mark = true;
  for( size_t i = 0 ; i < tGate->fanIn.size() ; i ++ )
    go( tGate->fanIn[ i ] );
}


void CirMgr::buildEdge( CirGate* _i, CirGate* _o, int inv , int iid , int oid ){
  _o->addIn( _i , iid , inv );
  if( _i ) _i->addOut( _o , oid , inv );
}

/**********************************************************/
/*   class CirMgr member functions for circuit printing   */
/**********************************************************/
/*********************
  Circuit Statistics
  ==================
  PI          20
  PO          12
  AIG        130
  ------------------
  Total      162
 *********************/
void
CirMgr::printSummary(){

  cout << endl;
  cout << "Circuit Statistics" << endl;
  cout << "==================" << endl;

  int _isz = nI;
  int _osz = nO;
  int _asz = 0;
  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( aList[ i ]->reach ) _asz ++;

  cout << "  PI   "; printf( "%9d\n" , _isz );
  cout << "  PO   "; printf( "%9d\n" , _osz );
  cout << "  AIG  "; printf( "%9d\n" , _asz );
  cout << "------------------" << endl;
  cout << "  Total"; printf( "%9d\n" , _isz + _osz + _asz );
}

void CirMgr::print( string& dat , int& idx ) const{
  cout << "[" << idx << "] " << dat << endl;
  idx ++;
}

void CirMgr::traverse( CirGate* tGate , int& idx ) const{
  if( !tGate ) return;
  if( tGate->getType() == 0 ) return;
  if( tGate->mark ) return;
  tGate->mark = true;
  string tdat = tGate->getTypeStr();
  while( tdat.length() < 4 ) tdat += " ";
  stringstream ss; ss << tGate->getId();
  string tmp; ss >> tmp;
  tdat += tmp;
  for( size_t i = 0 ; i < tGate->fanIn.size() ; i ++ ){
    traverse( tGate->fanIn[ i ] , idx );
    tdat += " ";
    if( !tGate->fanIn[ i ] ) tdat += "*";
    if( tGate->fanIn[ i ]->getType() == 0 ) tdat += "*";
    if( tGate->inv[ i ] ) tdat += "!";
    ss.clear();
    ss << tGate->idIn[ i ];
    ss >> tmp;
    tdat += tmp;
  }
  string ts = tGate->getSymbol();
  if( ts != "" ) tdat += " (" + ts + ")";
  print( tdat , idx );
}

void
CirMgr::printNetlist() const{
  cout << endl;
  int _idx = 0;
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    traverse( oList[ i ] , _idx );
  for( int i = 0 ; i <= nM + nO ; i ++ )
    if( mList[ i ] )
      mList[ i ]->mark = false;
}

void
CirMgr::printPIs() const{
  cout << "PIs of the circuit:";
  for( size_t i = 0 ; i < iList.size() ; i ++ ){
    cout << " ";
    cout << iList[ i ]->getId();
  }
  cout << endl;
}

void
CirMgr::printPOs() const{
  cout << "POs of the circuit:";
  for( size_t i = 0 ; i < oList.size() ; i ++ ){
    cout << " ";
    cout << oList[ i ]->getId();
  }
  cout << endl;
}

void
CirMgr::printFloatGates(){
  floating.clear();
  notusd.clear();
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    if( !oList[ i ]->fanIn[ 0 ] ||
        oList[ i ]->fanIn[ 0 ]->getType() == 0 )
      floating.push_back( oList[ i ]->getId() );

  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( ( !aList[ i ]->fanIn[ 0 ] || aList[ i ]->fanIn[ 0 ]->getType() == 0 ) ||
        ( !aList[ i ]->fanIn[ 1 ] || aList[ i ]->fanIn[ 1 ]->getType() == 0 ) )
      floating.push_back( aList[ i ]->getId() );

  for( size_t i = 1 ; i < mList.size() ; i ++ )
    if( mList[ i ] &&
        mList[ i ]->getType() != 2 &&
        mList[ i ]->fanOut.size() == 0 )
      notusd.push_back( i );
  
  if( floating.size() ){
    cout << "Gates with floating fanin(s):";
    for( size_t i = 0 ; i < floating.size() ; i ++ ){
      cout << " ";
      cout << floating[ i ];
    }
    cout << endl;
  }
  if( notusd.size() ){
    cout << "Gates defined but not used  :";
    for( size_t i = 0 ; i < notusd.size() ; i ++ ){
      cout << " ";
      cout << notusd[ i ];
    }
    cout << endl;
  }
}

void
CirMgr::printFECPairs() const{
  int ii = 0;
  for( int al = 0 , ar = 0 ; al < _g ; al = ar ){
    ar = al + 1;
    while( ar < _g && !gates[ ar ].second ) ar ++;
    if( ar - al <= 1 ) continue;
    bool zero = false;
    vector<int> vv;
    vv.push_back( gates[ al ].first->getId() );
    for( int i = al + 1 ; i < ar ; i ++ )
      if( gates[ i ].first->result ==
          gates[ al ].first->result )
        vv.push_back( +gates[ i ].first->getId() );
      else{
        if( gates[ i ].first->getId() == 0 )
          zero = true;
        vv.push_back( -gates[ i ].first->getId() );
      }
    sort( vv.begin() , vv.end() , cmp2 );
    if( ( vv[ 0 ] < 0 ) || zero ){
      for( size_t i = 0 ; i < vv.size() ; i ++ )
        vv[ i ] = -vv[ i ];
    }
    cout << "[" << ii ++ << "]";
    for( size_t i = 0 ; i < vv.size() ; i ++ ){
      cout << " "; if( vv[ i ] < 0 ) cout << "!";
      cout << abs( vv[ i ] );
    }
    cout << endl;
  }
}

void CirMgr::goWriteAig(CirGate* tGate, ostream& outfile){
  if( !tGate || tGate->mark ) return;
  tGate->mark = true;
  for( size_t i = 0 ; i < tGate->fanIn.size() ; i ++ )
    goWriteAig( tGate->fanIn[ i ] , outfile );
  if( tGate->getType() == 3 ){
    stringstream ss;
    string ts;
    ss << tGate->getId() * 2 << " ";
    ss << tGate->idIn[ 0 ] * 2 + tGate->inv[ 0 ] << " ";
    ss << tGate->idIn[ 1 ] * 2 + tGate->inv[ 1 ];
    getline( ss , ts );
    outfile << ts << endl;
  }
}

void
CirMgr::writeAag(ostream& outfile){
  nA = 0;
  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( aList[ i ] && aList[ i ]->reach )
      aList[ nA ++ ] = aList[ i ];
  aList.resize( nA );
  outfile << "aag " << nM << " " << nI << " 0 " << nO << " " << nA << endl;
  for( size_t i = 0 ; i < iList.size() ; i ++ )
    outfile << iList[ i ]->getId() * 2 << endl;
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    outfile << oList[ i ]->idIn[ 0 ] * 2 + oList[ i ]->inv[ 0 ] << endl;
  for( size_t i = 0 ; i < aList.size() ; i ++ ){
    outfile << aList[ i ]->getId() * 2 << " ";
    outfile << aList[ i ]->idIn[ 0 ] * 2 + aList[ i ]->inv[ 0 ] << " ";
    outfile << aList[ i ]->idIn[ 1 ] * 2 + aList[ i ]->inv[ 1 ] << "\n";
  }

  for( size_t i = 0 ; i < iList.size() ; i ++ ){
    if( iList[ i ]->getSymbol() == "" ) continue;
    outfile << "i" << i << " " << iList[ i ]->getSymbol() << endl;
  }
  for( size_t i = 0 ; i < oList.size() ; i ++ ){
    if( oList[ i ]->getSymbol() == "" ) continue;
    outfile << "o" << i << " " << oList[ i ]->getSymbol() << endl;
  }
  outfile << "c\n";
  outfile << "AAG output by eddy1021\n";
}

void
CirMgr::goCount( CirGate* g ){
  if( !g || g->stap == stap ) return;
  g->stap = stap;
  g->mark = false;
  if( g->getType() == 1 ) countI ++;
  if( g->getType() == 3 ) countA ++;
  maxId = std::max( maxId ,(int) g->getId() );
  for( size_t i = 0 ; i < g->fanIn.size() ; i ++ )
    goCount( g->fanIn[ i ] );
}
void
CirMgr::writeGate( ostream& outfile, CirGate *g ){
  stap ++; countI = countA = 0; maxId = g->getId();
  goCount( g );
  stringstream ss;
  ss << "aag " << maxId << " " << countI << " " << 0
     << " " << 1 << " " << countA << endl;
  string ts; getline( ss , ts );
  outfile << ts << endl;
  for( size_t i = 0 ; i < iList.size() ; i ++ )
    if( iList[ i ] && iList[ i ]->stap == stap )
      outfile << iList[ i ]->getId() * 2 << endl;
  outfile << g->getId() * 2 << endl;
  goWriteAig( g , outfile );
  for( size_t i = 0 ; i < iList.size() ; i ++ ){
    if( iList[ i ]->stap != stap ||
        iList[ i ]->getSymbol() == "" ) continue;
    int tid = iList[ i ]->getId() - 1;
    outfile << "i" << tid << " " << iList[ i ]->getSymbol() << endl;
  }
  outfile << "o0 " << g->getId() << endl;
  outfile << "c\nAAG output by eddy1021\n";
}

void
CirMgr::goMark( CirGate* g ){
  if( !g || g->reach ) return;
  g->reach = true;
  for( size_t i = 0 ; i < g->fanIn.size() ; i ++ )
    goMark( g->fanIn[ i ] );
}

void
CirMgr::markReachableGates(){
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    goMark( oList[ i ] );
  mList[ 0 ]->reach = true;
}

void
CirMgr::TopoSort(){
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] && mList[ i ]->reach )
      mList[ i ]->indeg = 0;
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] && mList[ i ]->reach )
      for( size_t j = 0 ; j < mList[ i ]->fanOut.size() ; j ++ )
        if( mList[ i ]->fanOut[ j ]->reach )
          mList[ i ]->fanOut[ j ]->indeg ++;
  int topoId = 0;
  queue<CirGate*> Q;
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] && mList[ i ]->reach &&
        mList[ i ]->indeg == 0 )
      Q.push( mList[ i ] );
  while( Q.size() ){
    CirGate* tGate = Q.front(); Q.pop();
    tGate->topoId = ++ topoId;
    for( size_t i = 0 ; i < tGate->fanOut.size() ; i ++ ){
      tGate->fanOut[ i ]->indeg --;
      if( tGate->fanOut[ i ]->indeg == 0 )
        Q.push( tGate->fanOut[ i ] );
    }
  }
  mList[ 0 ]->topoId = 0;
}

void
CirMgr::buildfecg( int gid ){
  mList[ gid ]->fecg.clear();
  for( int al = 0 , ar = 0 ; al < _g ; al = ar ){
    ar = al + 1;
    while( !gates[ ar ].second ) ar ++;
    bool got = false;
    for( int j = al ; j < ar ; j ++ )
      if( (int)gates[ j ].first->getId() == gid ){
        got = true;
        break;
      }
    if( got ){
      for( int j = al ; j < ar ; j ++ )
        if( (int)gates[ j ].first->getId() != gid ){
          if( gates[ j ].first->result != mList[ gid ]->result )
            mList[ gid ]->fecg.push_back( gates[ j ].first->getId() * 2 + 1 );
          else
            mList[ gid ]->fecg.push_back( gates[ j ].first->getId() * 2 );
        }
      break;
    }
  }
}
