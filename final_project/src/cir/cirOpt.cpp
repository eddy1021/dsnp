/****************************************************************************
  FileName     [ cirSim.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define cir optimization functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#include <cassert>
#include <queue>
#include "cirMgr.h"
#include "cirGate.h"
#include "util.h"

using namespace std;

// TODO: Please keep "CirMgr::sweep()" and "CirMgr::optimize()" for cir cmd.
//       Feel free to define your own variables or functions

/*******************************/
/*   Global variable and enum  */
/*******************************/

/**************************************/
/*   Static varaibles and functions   */
/**************************************/

/**************************************************/
/*   Public member functions about optimization   */
/**************************************************/
// Remove unused gates
// DFS list should NOT be changed
// UNDEF, float and unused list may be changed
void
CirMgr::sweep(){
  int _sz = 0;
  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( aList[ i ] && aList[ i ]->reach )
      aList[ _sz ++ ] = aList[ i ];
  aList.resize( _sz );

  for( size_t i = 0 ; i < mList.size() ; i ++ ){
    if( mList[ i ] && !mList[ i ]->reach &&
        ( mList[ i ]->getType() == 0 ||
          mList[ i ]->getType() == 3 ) ){
      if( !mList[ i ]->swept ){
        mList[ i ]->swept = true;
        cout << "Sweeping: " << mList[ i ]->getTypeStr() << "(";
        cout << mList[ i ]->getId() << ") removed..." << endl;
      }
    }else if( mList[ i ] ){
      int _sz2 = 0;
      for( size_t j = 0 ; j < mList[ i ]->fanOut.size() ; j ++ ){
        CirGate* nxtGate = mList[ i ]->fanOut[ j ];
        if( nxtGate && !nxtGate->reach ) {} // useless fanout
        else{
          mList[ i ]->idOut[ _sz2 ] = mList[ i ]->idOut[ j ];
          mList[ i ]->onv[ _sz2 ] = mList[ i ]->onv[ j ];
          mList[ i ]->fanOut[ _sz2 ++ ] = nxtGate;
        }
      }
      mList[ i ]->idOut.resize( _sz2 );
      mList[ i ]->onv.resize( _sz2 );
      mList[ i ]->fanOut.resize( _sz2 );
    }
  }
}

// Recursively simplifying from POs;
// _dfsList needs to be reconstructed afterwards
// UNDEF gates may be delete if its fanout becomes empty...
void
CirMgr::eliminate( CirGate* ori ){
  if( ori->getType() != 0 &&
      ori->getType() != 3 ) return;
  ori->reach = false;
  ori->swept = true;
  elicount ++;

  for( size_t i = 0 ; i < ori->fanIn.size() ; i ++ ){
    CirGate* preGate = ori->fanIn[ i ];
    if( preGate && preGate->reach ){
      int _sz = 0;
      for( size_t j = 0 ; j < preGate->fanOut.size() ; j ++ )
        if( preGate->fanOut[ j ] && 
            preGate->fanOut[ j ]->reach ){
          preGate->fanOut[ _sz ] = preGate->fanOut[ j ];
          preGate->idOut[ _sz ] = preGate->idOut[ j ];
          preGate->onv[ _sz ] = preGate->onv[ j ];
          _sz ++;
        }
      preGate->fanOut.resize( _sz );
      preGate->idOut.resize( _sz );
      preGate->onv.resize( _sz );
      if( _sz == 0 ) eliminate( preGate );
    }
  }
  ori->fanOut.clear();
  ori->idOut.clear();
  ori->onv.clear();
  ori->fanIn.clear();
  ori->idIn.clear();
  ori->inv.clear();
}

void
CirMgr::replace( CirGate* ori , int gid , int inv , int oid , string pre ){
  if( mList[ oid ]->topoId < mList[ gid ]->topoId ){
    swap( gid , oid );
    ori = mList[ oid ];
  }

  cout << pre << ": " << gid << " merging ";
  if( inv ) cout << "!";
  cout << oid << "..." << endl;
  for( size_t i = 0 ; i < ori->fanOut.size() ; i ++ ){
    CirGate* nxtGate = ori->fanOut[ i ];
    if( !nxtGate || !nxtGate->reach ) continue;
    mList[ gid ]->fanOut.push_back( nxtGate );
    mList[ gid ]->idOut.push_back( ori->idOut[ i ] );
    mList[ gid ]->onv.push_back( ( inv + ori->onv[ i ] ) & 1 );
    for( size_t j = 0 ; j < nxtGate->fanIn.size() ; j ++ )
      if( nxtGate->idIn[ j ] == oid ){
        nxtGate->fanIn[ j ] = mList[ gid ];
        nxtGate->idIn[ j ] = gid;
        nxtGate->inv[ j ] = ( nxtGate->inv[ j ] + inv ) & 1;
      }
  }
  ori->reach = false;
  ori->swept = true;
  for( size_t i = 0 ; i < ori->fanIn.size() ; i ++ ){
    CirGate* preGate = ori->fanIn[ i ];
    if( !preGate || !preGate->reach ) continue;
    int _sz = 0;
    for( size_t j = 0 ; j < preGate->fanOut.size() ; j ++ ){
      if( preGate->fanOut[ j ] && 
          preGate->idOut[ j ] != oid ){
        preGate->fanOut[ _sz ] = preGate->fanOut[ j ];
        preGate->idOut[ _sz ] = preGate->idOut[ j ];
        preGate->onv[ _sz ] = preGate->onv[ j ];
        _sz ++;
      }
    }
    preGate->fanOut.resize( _sz );
    preGate->idOut.resize( _sz );
    preGate->onv.resize( _sz );
  }
  if( ori->getType() == 3 ){
    ori->fanOut.clear(); ori->idOut.clear(); ori->onv.clear();
    ori->fanIn.clear();  ori->idIn.clear();  ori->inv.clear();
    ori->reach = false;
  }
  eliminate( ori );
}

void
CirMgr::optimize(){
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] )
      mList[ i ]->indeg = 0;
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ]->reach )
      for( size_t j = 0 ; j < mList[ i ]->fanOut.size() ; j ++ )
        if( mList[ i ]->fanOut[ j ] &&
            mList[ i ]->fanOut[ j ]->reach )
          mList[ i ]->fanOut[ j ]->indeg ++;
  queue< CirGate* > Q;
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ]->reach && mList[ i ]->indeg == 0 )
      Q.push( mList[ i ] );
  while( Q.size() ){
    CirGate* nGate = Q.front(); Q.pop();
    if( nGate->getType() == 2 ) continue;
    int nIn1 = -1, nIn2 = -1;
    for( size_t i = 0 ; i < nGate->fanIn.size() ; i ++ ){
      int nTmp = ( nGate->idIn[ i ] << 1 ) + nGate->inv[ i ];
      if( i == 0 ) nIn1 = nTmp;
      else nIn2 = nTmp;
    }
    if( nIn1 != -1 && nIn2 != -1 ){
      if( nIn1 == ( nIn2 ^ 1 ) ) // inverse
        replace( nGate , 0 , 0 , nGate->getId() , "Simplifying" );
      else if( nIn1 == 0 || nIn2 == 0 ) // const 0
        replace( nGate , 0 , 0 , nGate->getId() , "Simplifying" );
      else if( nIn1 == nIn2 ) // same
        replace( nGate , nGate->idIn[ 0 ] , nGate->inv[ 0 ] , nGate->getId() , "Simplifying" );
      else if( nIn1 == 1 ) // const 1
        replace( nGate , nGate->idIn[ 1 ] , nGate->inv[ 1 ] , nGate->getId() , "Simplifying" );
      else if( nIn2 == 1 ) // const 1
        replace( nGate , nGate->idIn[ 0 ] , nGate->inv[ 0 ] , nGate->getId() , "Simplifying" );
    }
    for( size_t i = 0 ; i < nGate->fanOut.size() ; i ++ )
      if( nGate->fanOut[ i ]->reach ){
        nGate->fanOut[ i ]->indeg --;
        if( nGate->fanOut[ i ]->indeg == 0 )
          Q.push( nGate->fanOut[ i ] );
      }
  }

  int _sz = 0;
  for( size_t i = 0 ; i < aList.size() ; i ++ ){
    if( !aList[ i ] || !aList[ i ]->reach ){
      // good bye useless gate
    }else aList[ _sz ++ ] = aList[ i ];
  }
  aList.resize( _sz );
}

/***************************************************/
/*   Private member functions about optimization   */
/***************************************************/
