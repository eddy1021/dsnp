/****************************************************************************
  FileName     [ cirGate.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define class CirAigGate member functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdarg.h>
#include <cassert>
#include "cirGate.h"
#include "cirMgr.h"
#include "util.h"

using namespace std;

// TODO: Keep "CirGate::reportGate()", "CirGate::reportFanin()" and
//       "CirGate::reportFanout()" for cir cmds. Feel free to define
//       your own variables and functions.

extern CirMgr *cirMgr;

/**************************************/
/*   class CirGate member functions   */
/**************************************/
void
CirGate::reportGate() const{
  printBlock();

  string ts = getTypeStr();
  if( ts.length() > 4 ) ts += "(0), line 0";
  else{
    stringstream ss; ss << id;
    string tmp; ss >> tmp;
    ts += "(" + tmp + ")"; 
    if( symbol != "" )
      ts += "\"" + symbol + "\"";
    ts += ", line ";
    ss.clear();
    ss << lineNo; ss >> tmp;
    ts += tmp;
  }
  cout << "= ";
  cout << setw( 46 ) << left << ts;
  cout << " =\n";

  cout << "= FECs:";
 
  ts = "";
  for( size_t i = 0 ; i < fecg.size() ; i ++ ){
    stringstream ss;
    if( fecg[ i ] % 2 ) ss << "!";
    ss << fecg[ i ] / 2;
    string tmp; ss >> tmp;
    ts += " " + tmp;
  }
  cout << setw( 41 ) << left << ts;

  cout << " =\n";

  cout << "= Value: ";
  for( int i = 31 ; i >= 0 ; i -- ){
    if( ( result >> i ) & 1llu ) cout << "1";
    else cout << "0";
    if( i % 4 == 0 && i != 0 ) cout << "_";
  }
  cout << " =\n";

  printBlock();
}

void CirGate::iClean(){
  if( !mark ) return;
  mark = false;
  for( size_t i = 0 ; i < fanIn.size() ; i ++ )
    if( fanIn[ i ] )
      fanIn[ i ]->iClean();
}
void CirGate::oClean(){
  if( !mark ) return;
  mark = false;
  for( size_t i = 0 ; i < fanOut.size() ; i ++ )
    if( fanOut[ i ] )
      fanOut[ i ]->oClean();
}


void CirGate::MyreportFanin(int level, string pre, int _inv){
  cout << pre;
  if( _inv ) cout << "!";
  cout << getTypeStr() << " ";
  stringstream ss; ss << id;
  string tmp; ss >> tmp;
  cout << tmp << endl;

  if( level > 0 ){
    if( mark ){
      cout << " (*)";
      return;
    }
    mark = true;
    for( size_t i = 0 ; i < fanIn.size() ; i ++ )
      if( fanIn[ i ] ){
        fanIn[ i ]->MyreportFanin( level - 1 , pre + "  " , inv[ i ] );
      }else{
        cout << pre << "  ";
        if( _inv ) cout << "!";
        cout << "UNDEF" << endl;
      }
  }
}

void
CirGate::reportFanin(int level){
  assert (level >= 0);
  MyreportFanin( level , "" , 0 );
  iClean();
}

void CirGate::MyreportFanout(int level, string pre, int _inv){
  cout << pre;
  if( _inv ) cout << "!";
  cout << getTypeStr() << " ";
  stringstream ss; ss << id;
  string tmp; ss >> tmp;
  cout << tmp << endl;

  if( level > 0 ){
    if( mark ){
      cout << " (*)";
      return;
    }
    mark = true;
    for( size_t i = 0 ; i < fanOut.size() ; i ++ )
      if( fanOut[ i ] ){
        fanOut[ i ]->MyreportFanout( level - 1 , pre + "  " , onv[ i ] );
      }else{
        cout << pre << "  ";
        if( _inv ) cout << "!";
        cout << "UNDEF" << endl;
      }
  }
}

void
CirGate::reportFanout(int level){
  assert (level >= 0);
  MyreportFanout( level , "" , 0 );
  oClean();
}

