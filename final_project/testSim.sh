for i in {1..15}
do
  echo $i"processing..."
  ./fraig -F "dofiles/doSim"$i
  ./ref/fraig -F "dofiles/refdoSim"$i
  diff my.out ref.out
done
