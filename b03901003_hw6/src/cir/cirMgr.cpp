/****************************************************************************
  FileName     [ cirMgr.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define cir manager functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <ctype.h>
#include <cassert>
#include <cstring>
#include "cirMgr.h"
#include "cirGate.h"
#include "util.h"
#include <sstream>

using namespace std;

// TODO: Implement memeber functions for class CirMgr

CirMgr::~CirMgr(){
  for( size_t i = 0 ; i < mList.size() ; i ++ )
    if( mList[ i ] )
      delete mList[ i ];
}

/*******************************/
/*   Global variable and enum  */
/*******************************/
CirMgr* cirMgr = 0;

enum CirParseError {
  EXTRA_SPACE,
  MISSING_SPACE,
  ILLEGAL_WSPACE,
  ILLEGAL_NUM,
  ILLEGAL_IDENTIFIER,
  ILLEGAL_SYMBOL_TYPE,
  ILLEGAL_SYMBOL_NAME,
  MISSING_NUM,
  MISSING_IDENTIFIER,
  MISSING_NEWLINE,
  MISSING_DEF,
  CANNOT_INVERTED,
  MAX_LIT_ID,
  REDEF_GATE,
  REDEF_SYMBOLIC_NAME,
  REDEF_CONST,
  NUM_TOO_SMALL,
  NUM_TOO_BIG,

  DUMMY_END
};

/**************************************/
/*   Static varaibles and functions   */
/**************************************/
static unsigned lineNo = 0;  // in printint, lineNo needs to ++
static unsigned colNo  = 0;  // in printing, colNo needs to ++
static char buf[1024];
static string errMsg;
static int errInt;
static CirGate *errGate;

  static bool
parseError(CirParseError err)
{
  switch (err) {
    case EXTRA_SPACE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Extra space character is detected!!" << endl;
      break;
    case MISSING_SPACE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Missing space character!!" << endl;
      break;
    case ILLEGAL_WSPACE: // for non-space white space character
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Illegal white space char(" << errInt
        << ") is detected!!" << endl;
      break;
    case ILLEGAL_NUM:
      cerr << "[ERROR] Line " << lineNo+1 << ": Illegal "
        << errMsg << "!!" << endl;
      break;
    case ILLEGAL_IDENTIFIER:
      cerr << "[ERROR] Line " << lineNo+1 << ": Illegal identifier \""
        << errMsg << "\"!!" << endl;
      break;
    case ILLEGAL_SYMBOL_TYPE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Illegal symbol type (" << errMsg << ")!!" << endl;
      break;
    case ILLEGAL_SYMBOL_NAME:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Symbolic name contains un-printable char(" << errInt
        << ")!!" << endl;
      break;
    case MISSING_NUM:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Missing " << errMsg << "!!" << endl;
      break;
    case MISSING_IDENTIFIER:
      cerr << "[ERROR] Line " << lineNo+1 << ": Missing \""
        << errMsg << "\"!!" << endl;
      break;
    case MISSING_NEWLINE:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": A new line is expected here!!" << endl;
      break;
    case MISSING_DEF:
      cerr << "[ERROR] Line " << lineNo+1 << ": Missing " << errMsg
        << " definition!!" << endl;
      break;
    case CANNOT_INVERTED:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": " << errMsg << " " << errInt << "(" << errInt/2
        << ") cannot be inverted!!" << endl;
      break;
    case MAX_LIT_ID:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Literal \"" << errInt << "\" exceeds maximum valid ID!!"
        << endl;
      break;
    case REDEF_GATE:
      cerr << "[ERROR] Line " << lineNo+1 << ": Literal \"" << errInt
        << "\" is redefined, previously defined as "
        << errGate->getTypeStr() << " in line " << errGate->getLineNo()
        << "!!" << endl;
      break;
    case REDEF_SYMBOLIC_NAME:
      cerr << "[ERROR] Line " << lineNo+1 << ": Symbolic name for \""
        << errMsg << errInt << "\" is redefined!!" << endl;
      break;
    case REDEF_CONST:
      cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
        << ": Cannot redefine const (" << errInt << ")!!" << endl;
      break;
    case NUM_TOO_SMALL:
      cerr << "[ERROR] Line " << lineNo+1 << ": " << errMsg
        << " is too small (" << errInt << ")!!" << endl;
      break;
    case NUM_TOO_BIG:
      cerr << "[ERROR] Line " << lineNo+1 << ": " << errMsg
        << " is too big (" << errInt << ")!!" << endl;
      break;
    default: break;
  }
  return false;
}

bool isNum( char ctmp ){
  return ctmp >= '0' && ctmp <= '9';
}

/**************************************************************/
/*   class CirMgr member functions for circuit construction   */
/**************************************************************/
bool
CirMgr::readCircuit(const string& fileName){
  nM = nI = nL = nO = nA = usdA = 0;
  iList.clear(); oList.clear();
  aList.clear(); mList.clear();
  floating.clear(); notusd.clear();
  ifstream in( fileName.data() , ifstream::in );
  if( !in.is_open() ) return false;
  string inputBuf , dump;
  bool header = false;
  int got = 0, lno = 0, po = 0;
  vector<int> st , i1 , i2;
  while( getline( in , inputBuf ) ){
    lineNo = lno; colNo = 0;
    if( inputBuf[ 0 ] == ' ' )
      return parseError( EXTRA_SPACE );
    if( inputBuf[ 0 ] == (char)9 ){
      errInt = 9;
      return parseError( ILLEGAL_WSPACE );
    }
    lno ++; 
    int inputLen = inputBuf.length();
    if( inputLen > 0 && inputBuf[ 0 ] == 'c' ){
      if( inputLen > 1 ){
        colNo = 1;
        return parseError( MISSING_NEWLINE ); 
      }
      break;
    }
    if( !header ){
      header = true;
      dump = "";
      for( int i = 0 ; i < inputLen ; i ++ )
        if( inputBuf[ i ] == ' ' ) break;
        else dump += inputBuf[ i ];
      if( strcmp( dump.data() , "aag" ) ){
        errMsg = dump;
        return parseError( ILLEGAL_IDENTIFIER );
      }
      if( inputBuf.length() < 4 ){
        colNo = inputBuf.length();
        errMsg = "number of vars!!";
        return parseError( MISSING_NUM );
      }
      if( inputBuf[ 3 ] != ' ' ){
        colNo = 3;
        return parseError( MISSING_SPACE );
      }
      for( int i = 4 ; i < inputLen ; i ++ ){
        colNo = i;
        if( inputBuf[ i - 1 ] == ' ' ){
          if( inputBuf[ i ] == ' ' )
            return parseError( EXTRA_SPACE );
          else if( inputBuf[ i ] == (char)9 )
            return parseError( ILLEGAL_WSPACE );
        }else if( inputBuf[ i ] == (char)9 )
          return parseError( MISSING_SPACE );
      }
      int tgot = 0;
      for( int i = 4 ; i < inputLen ; i ++ )
        if( isNum( inputBuf[ i ] ) ){
          if( !isNum( inputBuf[ i - 1 ] ) ) tgot ++;
        }else if( inputBuf[ i ] != ' ' ){
          if( inputBuf[ i - 1 ] == ' ' ) tgot ++;
          errMsg = "number of ";
          if( tgot == 1 ) errMsg += "vars";
          if( tgot == 2 ) errMsg += "PIs";
          if( tgot == 3 ) errMsg += "PLs";
          if( tgot == 4 ) errMsg += "POs";
          if( tgot == 5 ) errMsg += "AIGs";
          errMsg += "(";
          int _ptr = i;
          while( _ptr >= 0 && inputBuf[ _ptr ] != ' ' ) _ptr --;
          _ptr ++;
          while( _ptr < inputLen && inputBuf[ _ptr ] != ' ' ) errMsg += inputBuf[ _ptr ++ ];
          errMsg += ")";
          colNo = i;
          return parseError( ILLEGAL_NUM );
        }else if( tgot == 5 ){
          colNo = i;
          return parseError( MISSING_NEWLINE );
        }
      if( tgot < 5 ){
        colNo = inputLen;
        errMsg = "number of vars!!";
        return parseError( MISSING_NUM );
      }
      stringstream ss; ss << inputBuf;
      ss >> dump >> nM >> nI >> nL >> nO >> nA;
      if( nI + nL + nA > nM ){
        errInt = nM;
        errMsg = "Num of variables";
        nI = nL = nA = nM = nO = 0;
        return parseError( NUM_TOO_SMALL );
      }
      if( nL ){
        cerr << "[ERROR] Line " << lineNo+1 << ": Illegal latches!!\n";
        return false;
      }
      mList.resize( nM + 1 );
      mList[ 0 ] = new CirGate( 0 , 0 , 4 );
    }else if( got < nI ){
      int tgot = 0; bool nlm = false;
      for( int i = 0 ; i < inputLen ; i ++ )
        if( isNum( inputBuf[ i ] ) ){
          if( !isNum( inputBuf[ i - 1 ] ) ) tgot ++;
        }else if( tgot == 1 ){
          colNo = i; nlm = true; break;
        }
      if( inputLen == 0 ){
        colNo = 0;
        errMsg = "PI literal ID";
        return parseError( MISSING_NUM );
      }
      stringstream ss; ss << inputBuf;
      int tid; ss >> tid;
      if( tid < 2 ){
        colNo = 0;
        errInt = tid;
        return parseError( REDEF_CONST );
      }
      if( tid & 1 ){
        colNo = 0;
        errMsg = "PI";
        errInt = tid;
        return parseError( CANNOT_INVERTED );
      }
      if( tid / 2 > nM ){
        colNo = 0;
        errInt = tid;
        return parseError( MAX_LIT_ID );
      }
      if( mList[ tid / 2 ] ){
        errInt = tid;
        errGate = mList[ tid / 2 ];
        return parseError( REDEF_GATE );
      }
      if( nlm )
        return parseError( MISSING_NEWLINE );
      CirGate* tGate = new CirGate( tid / 2 , lno , 1 );
      iList.push_back( tGate );
      mList[ tid / 2 ] = tGate;
      got ++;
    }else if( got < nI + nL ){
      stringstream ss; ss << inputBuf;
      int tid; ss >> tid;
      //CirGate* tGate = new CirGate( tid , lno );
      //lList.push_back( tGate );
      got ++;
    }else if( got < nI + nL + nO ){
      int tgot = 0; bool nlm = false;
      for( int i = 0 ; i < inputLen ; i ++ )
        if( isNum( inputBuf[ i ] ) ){
          if( !isNum( inputBuf[ i - 1 ] ) ) tgot ++;
        }else if( tgot == 1 ){
          colNo = i; nlm = true; break;
        }
      if( inputLen == 0 ){
        colNo = 0;
        errMsg = "PO literal ID";
        return parseError( MISSING_NUM );
      }
      if( nlm )
        return parseError( MISSING_NEWLINE );
      stringstream ss; ss << inputBuf;
      int tid; ss >> tid; po ++;
      if( tid / 2 > nM ){
        colNo = 0;
        errInt = tid;
        return parseError( MAX_LIT_ID );
      }
      int nid = nM + po;
      CirGate* tGate = new CirGate( nid , lno , 2 );
      oList.push_back( tGate );
      mList.push_back( tGate );
      st.push_back( tid );
      got ++;
    }else if( got < nI + nL + nO + nA ){
      for( int i = 0 ; i < inputLen ; i ++ ){
        colNo = i;
        if( i && inputBuf[ i - 1 ] == ' ' ){
          if( inputBuf[ i ] == ' ' )
            return parseError( EXTRA_SPACE );
          else if( inputBuf[ i ] == (char)9 )
            return parseError( ILLEGAL_WSPACE );
        }else if( inputBuf[ i ] == (char)9 )
          return parseError( MISSING_SPACE );
      }
      int tgot = 0; bool nlm = false;
      int tpos[3]={};
      for( int i = 0 ; i <= inputLen ; i ++ )
        if( i == inputLen ){
          if( tgot < 3 ){
            colNo = i;
            return parseError( MISSING_SPACE );
          }
        }else if( isNum( inputBuf[ i ] ) ){
          if( i == 0 || !isNum( inputBuf[ i - 1 ] ) ){
            tpos[ tgot ] = i;
            tgot ++;
          }
        }else if( tgot == 3 ){
          colNo = i; nlm = true; break;
        }
      if( inputLen == 0 ){
        colNo = 0;
        errMsg = "AIG literal ID";
        return parseError( MISSING_NUM );
      }
      stringstream ss; ss << inputBuf;
      int tid , in1 , in2;
      ss >> tid >> in1 >> in2;
      if( tid < 2 ){
        colNo = 0;
        errInt = tid;
        return parseError( REDEF_CONST );
      }
      if( tid & 1 ){
        colNo = 0;
        errMsg = "AIG";
        errInt = tid;
        return parseError( CANNOT_INVERTED );
      }
      if( tid / 2 > nM ){
        colNo = 0;
        errInt = tid;
        return parseError( MAX_LIT_ID );
      }
      if( mList[ tid / 2 ] ){
        errInt = tid;
        errGate = mList[ tid / 2 ];
        return parseError( REDEF_GATE );
      }
      if( in1 / 2 > nM ){
        colNo = tpos[ 1 ];
        errInt = in1;
        return parseError( MAX_LIT_ID );
      }
      if( in2 / 2 > nM ){
        colNo = tpos[ 2 ];
        errInt = in2;
        return parseError( MAX_LIT_ID );
      }
      if( nlm )
        return parseError( MISSING_NEWLINE );
      CirGate* tGate = new CirGate( tid / 2 , lno , 3 );
      aList.push_back( tGate );
      mList[ tid / 2 ] = tGate;
      i1.push_back( in1 );
      i2.push_back( in2 );
      got ++;
    }else{
      stringstream ss; ss << inputBuf;
      int tgot = 0; bool nlm = false;
      for( int i = 0 ; i < inputLen ; i ++ ){
        colNo = i;
        if( inputBuf[ i ] == (char)9 ){
          if( i && inputBuf[ i - 1 ] == ' ' )
            return parseError( ILLEGAL_WSPACE );
          return parseError( MISSING_SPACE );
        }else if( (int)inputBuf[ i ] == ' ' ){
          // if( i && inputBuf[ i - 1 ] == ' ' )
            // return parseError( EXTRA_SPACE );
        }else if( (int)inputBuf[ i ] < 32 ||
                  (int)inputBuf[ i ] > 126 ){
          colNo = i;
          errInt = (int)inputBuf[ i ];
          return parseError( ILLEGAL_SYMBOL_NAME );
        }else if( tgot == 2 ){
          colNo = i; nlm = true; break;
        }
      }
      if( inputBuf.length() > 1 ){
        colNo = 1;
        if( inputBuf[ 1 ] == ' ' )
          return parseError( EXTRA_SPACE );
        else if( inputBuf[ 1 ] == (char)9 ){
          errInt = 9;
          return parseError( ILLEGAL_WSPACE );
        }
      }
      string sid , sym;
      ss >> sid >> sym;
      int sidLen = sid.length();
      if( sidLen == 0 ){
        colNo = 0;
        errMsg = "";
        return parseError( ILLEGAL_SYMBOL_TYPE );
      }
      if( sid[ 0 ] != 'i' &&
          sid[ 0 ] != 'o' ){
        colNo = 0;
        errMsg = sid;
        errMsg.resize( 1 );
        return parseError( ILLEGAL_SYMBOL_TYPE );
      }
      for( int i = 1 ; i < sidLen ; i ++ )
        if( !isNum( sid[ i ] ) ){
          errMsg = "symbol index(" + sid.substr( 1 ) + ")";
          return parseError( ILLEGAL_NUM );
        }
      if( sym.length() == 0 ){
        cerr << "[ERROR] Line " << lineNo + 1 << ":";
        cerr << " Missing \"symbolic name\"!!\n";
        return false;
      }
      int nid = atoi( sid.substr( 1 ).data() );
      if( sid[ 0 ] == 'i' && nid >= nI ){
        errInt = nid;
        errMsg = "PI index";
        return parseError( NUM_TOO_BIG );
      }
      if( sid[ 0 ] == 'o' && nid >= nO ){
        errInt = nid;
        errMsg = "PO index";
        return parseError( NUM_TOO_BIG );
      }
      if( sid[ 0 ] == 'o' ) nid += nM;
      if( mList[ nid + 1 ] ){
        if( mList[ nid + 1 ]->getSymbol() != "" ){
          errMsg = sid[ 0 ];
          errInt = nid;
          if( sid[ 0 ] == 'o' ) nid -= nM;
          return parseError( REDEF_SYMBOLIC_NAME );
        }
        mList[ nid + 1 ]->setSymbol( sym );
      }
      if( nlm )
        return parseError( MISSING_NEWLINE );
    }
  }
  if( !header ){
    errMsg = "aag";
    return parseError( MISSING_IDENTIFIER );
  }
  if( got < nI ){
    lineNo = lno; errMsg = "PI";
    return parseError( MISSING_DEF );
  }
  if( got < nI + nL + nO ){
    lineNo = lno; errMsg = "PO";
    return parseError( MISSING_DEF );
  }
  if( got < nI + nL + nO + nA ){
    lineNo = lno; errMsg = "AIG";
    return parseError( MISSING_DEF );
  }
  for( int i = 0 ; i < nO ; i ++ ){
    buildEdge( mList[ st[ i ] / 2 ] , mList[ nM + i + 1 ] , st[ i ] & 1 , st[ i ] / 2 , nM + i + 1 );
    if( !mList[ st[ i ] / 2 ] )
      floating.push_back( nM + i + 1 );
  }
  for( size_t i = 0 ; i < aList.size() ; i ++ ){
    buildEdge( mList[ i1[ i ] / 2 ] , aList[ i ] , i1[ i ] & 1 , i1[ i ] / 2 , aList[ i ]->getId() );
    buildEdge( mList[ i2[ i ] / 2 ] , aList[ i ] , i2[ i ] & 1 , i2[ i ] / 2 , aList[ i ]->getId() );
    if( !mList[ i1[ i ] / 2 ] ||
        !mList[ i2[ i ] / 2 ] )
      floating.push_back( aList[ i ]->getId() );
  }
  for( int i = 1 ; i <= nM ; i ++ )
    if( mList[ i ] &&
        mList[ i ]->fanOut.size() == 0 )
      notusd.push_back( i );
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    go( oList[ i ] );
  usdA = 0;
  for( size_t i = 0 ; i < aList.size() ; i ++ )
    if( aList[ i ]->mark ){
      usdA ++;
      aList[ i ]->usd = true;
    }
  for( int i = 0 ; i <= nM + nO ; i ++ )
    if( mList[ i ] )
      mList[ i ]->mark = false;
  return true;
}

void CirMgr::go( CirGate* tGate ){
  if( !tGate ) return;
  if( tGate->mark ) return;
  tGate->mark = true;
  for( size_t i = 0 ; i < tGate->fanIn.size() ; i ++ )
    go( tGate->fanIn[ i ] );
}


void CirMgr::buildEdge( CirGate* _i, CirGate* _o, int inv , int iid , int oid ){
  _o->addIn( _i , iid , inv );
  if( _i ) _i->addOut( _o , oid , inv );
}

/**********************************************************/
/*   class CirMgr member functions for circuit printing   */
/**********************************************************/
void
CirMgr::printSummary() const{
  cout << endl;
  cout << "Circuit Statistics" << endl;
  cout << "==================" << endl;

  int _isz = (int)iList.size();
  int _osz = (int)oList.size();
  int _asz = (int)aList.size();

  cout << "  PI   "; printf( "%9d\n" , _isz );
  cout << "  PO   "; printf( "%9d\n" , _osz );
  cout << "  AIG  "; printf( "%9d\n" , _asz );
  cout << "------------------" << endl;
  cout << "  Total"; printf( "%9d\n" , _isz + _osz + _asz );
}

void CirMgr::print( string& dat , int& idx ) const{
  cout << "[" << idx << "] " << dat << endl;
  idx ++;
}

void CirMgr::traverse( CirGate* tGate , int& idx ) const{
  if( !tGate ) return;
  if( tGate->mark ) return;
  tGate->mark = true;
  string tdat = tGate->getTypeStr();
  while( tdat.length() < 4 ) tdat += " ";
  stringstream ss; ss << tGate->getId();
  string tmp; ss >> tmp;
  tdat += tmp;
  for( size_t i = 0 ; i < tGate->fanIn.size() ; i ++ ){
    traverse( tGate->fanIn[ i ] , idx );
    tdat += " ";
    if( !tGate->fanIn[ i ] ) tdat += "*";
    if( tGate->inv[ i ] ) tdat += "!";
    ss.clear();
    ss << tGate->idIn[ i ];
    ss >> tmp;
    tdat += tmp;
  }
  string ts = tGate->getSymbol();
  if( ts != "" ) tdat += " (" + ts + ")";
  print( tdat , idx );
}

void
CirMgr::printNetlist() const{
  cout << endl;
  int _idx = 0;
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    traverse( oList[ i ] , _idx );
  for( int i = 0 ; i <= nM + nO ; i ++ )
    if( mList[ i ] )
      mList[ i ]->mark = false;
}

void
CirMgr::printPIs() const{
  cout << "PIs of the circuit:";
  for( size_t i = 0 ; i < iList.size() ; i ++ ){
    cout << " ";
    cout << iList[ i ]->getId();
  }
  cout << endl;
}

void
CirMgr::printPOs() const{
  cout << "POs of the circuit:";
  for( size_t i = 0 ; i < oList.size() ; i ++ ){
    cout << " ";
    cout << oList[ i ]->getId();
  }
  cout << endl;
}

void
CirMgr::printFloatGates() const{
  if( floating.size() ){
    cout << "Gates with floating fanin(s):";
    for( size_t i = 0 ; i < floating.size() ; i ++ ){
      cout << " ";
      cout << floating[ i ];
    }
    cout << endl;
  }
  if( notusd.size() ){
    cout << "Gates defined but not used  :";
    for( size_t i = 0 ; i < notusd.size() ; i ++ ){
      cout << " ";
      cout << notusd[ i ];
    }
    cout << endl;
  }
}


void CirMgr::goWriteAig(CirGate* tGate, ostream& outfile) const{
  if( !tGate ) return;
  if( tGate->mark ) return;
  tGate->mark = true;
  for( size_t i = 0 ; i < tGate->fanIn.size() ; i ++ )
    goWriteAig( tGate->fanIn[ i ] , outfile );
  if( tGate->usd ){
    stringstream ss;
    string ts;
    int _id0 = tGate->idIn[ 0 ] * 2 +
               tGate->inv[ 0 ];
    int _id1 = tGate->idIn[ 1 ] * 2 +
               tGate->inv[ 1 ];
    ss << tGate->getId() * 2 << " ";
    ss << _id0 << " ";
    ss << _id1 << endl;
    getline( ss , ts );
    outfile << ts << endl;
  }
}

void CirMgr::writeAag(ostream& outfile) const{
  stringstream ss;
  ss << "aag " << nM << " " << nI << " " << nL
     << " " << nO << " " << usdA << endl;
  string ts;
  getline( ss , ts );
  outfile << ts << endl;
  for( size_t i = 0 ; i < iList.size() ; i ++ )
    outfile << iList[ i ]->getId() * 2 << endl;
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    outfile << oList[ i ]->idIn[ 0 ] * 2 +
               oList[ i ]->inv[ 0 ] << endl;
  for( size_t i = 0 ; i < oList.size() ; i ++ )
    goWriteAig( oList[ i ] , outfile );
  for( int i = 0 ; i <= nM + nO ; i ++ )
    if( mList[ i ] ) mList[ i ]->mark = false;
  for( size_t i = 0 ; i < iList.size() ; i ++ ){
    if( iList[ i ]->getSymbol() == "" ) continue;
    int tid = iList[ i ]->getId() - 1;
    outfile << "i" << tid << " " << iList[ i ]->getSymbol() << endl;
  }
  for( size_t i = 0 ; i < oList.size() ; i ++ ){
    if( oList[ i ]->getSymbol() == "" ) continue;
    int tid = i;
    outfile << "o" << tid << " " << oList[ i ]->getSymbol() << endl;
  }
  outfile << "c\n";
  outfile << "AAG output by eddy1021\n";

}
