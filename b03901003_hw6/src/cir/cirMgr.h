/****************************************************************************
  FileName     [ cirMgr.h ]
  PackageName  [ cir ]
  Synopsis     [ Define circuit manager ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef CIR_MGR_H
#define CIR_MGR_H

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

#include "cirDef.h"

extern CirMgr *cirMgr;

// TODO: Define your own data members and member functions
class CirMgr
{
 public:
  CirMgr(){}
  ~CirMgr();

  // Access functions
  // return '0' if "gid" corresponds to an undefined gate.
  CirGate* getGate(unsigned gid) const { return mList[ gid ]; }

  // Member functions about circuit construction
  bool readCircuit(const string&);
  void buildEdge( CirGate*,CirGate*,int,int,int );
  void go( CirGate* );
  // Member functions about circuit reporting
  void print( string&, int& )const;
  void traverse( CirGate* , int& ) const;
  void printSummary() const;
  void printNetlist() const;
  void printPIs() const;
  void printPOs() const;
  void printFloatGates() const;
  void goWriteAig(CirGate*,ostream&) const;
  void writeAag(ostream&) const;

 private:
  int nM , nI , nL , nO , nA;
  int usdA;
  GateList iList, oList, aList;
  GateList mList;
  vector<int> floating, notusd;
};

#endif // CIR_MGR_H
