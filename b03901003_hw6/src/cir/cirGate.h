/****************************************************************************
  FileName     [ cirGate.h ]
  PackageName  [ cir ]
  Synopsis     [ Define basic gate data structures ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef CIR_GATE_H
#define CIR_GATE_H

#include <string>
#include <vector>
#include <iostream>
#include "cirDef.h"

using namespace std;

class CirGate;

//------------------------------------------------------------------------
//   Define classes
//------------------------------------------------------------------------
// TODO: Define your own data members and member functions, or classes
const static string gateTypeName[5] ={
  "UNDEF","PI","PO","AIG","CONST" };
class CirGate
{
 public:
  CirGate() { gateType = 0; mark = usd = false; symbol = ""; }
  CirGate( int _id , int _lineNo , int type , string _sym = "" ):
    id(_id), lineNo( _lineNo ), gateType( type ), symbol( _sym ){
      mark = usd = false; symbol = "";
    }
  ~CirGate() {}

  // Basic access methods
  void setSymbol( string _sym ){
    symbol = _sym;
  }
  string getSymbol() const{
    return symbol;
  }
  string getTypeStr() const { return gateTypeName[ gateType ]; }
  unsigned getLineNo() const { return lineNo; }
  unsigned getId() const { return id; }
  void addIn( CirGate* tGate , int inId , int tinv = 0 ){
    fanIn.push_back( tGate );
    inv.push_back( tinv );
    idIn.push_back( inId );
  }
  void addOut( CirGate* tGate , int outId , int tinv = 0 ){
    fanOut.push_back( tGate );
    onv.push_back( tinv );
    idOut.push_back( outId );
  }

  // Printing functions
  void printGate() const;
  void reportGate() const;
  void MyreportFanin(int level, string pre,int _inv);
  void MyreportFanout(int level, string pre,int _inv);
  void iClean();
  void oClean();
  void reportFanin(int level);
  void reportFanout(int level);

  GateList fanIn, fanOut;
  vector<int> idIn, idOut;
  vector<int> inv, onv;
  bool mark, usd;
 private:
  unsigned id;
  unsigned lineNo, gateType;
  string symbol;
  void printBlock() const{
    cout << "==================================================" << endl;
  }
};

#endif // CIR_GATE_H
