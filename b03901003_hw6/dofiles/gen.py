for i in range( 1 , 7 ):
  f = open( "do" + str( i ) , "w" )
  f.write( "cirr tests.fraig/opt0" + str( i ) + ".aag\n" )
  f.write( "cirp -S\n" )
  f.write( "cirp -N\n" )
  f.write( "cirp -PI\n" )
  f.write( "cirp -PO\n" )
  f.write( "cirp -FL\n" )
  f.write( "cirw\n" )
  f.write( "q -f\n" )
  f.close()

for i in range( 1 , 16 ):
  f = open( "do" + str( i + 6 ) , "w" )
  if i < 10 :
    f.write( "cirr tests.fraig/sim0" + str( i ) + ".aag\n" )
  else:
    f.write( "cirr tests.fraig/sim" + str( i ) + ".aag\n" )
  f.write( "cirp -S\n" )
  f.write( "cirp -N\n" )
  f.write( "cirp -PI\n" )
  f.write( "cirp -PO\n" )
  f.write( "cirp -FL\n" )
  f.write( "cirw\n" )
  f.write( "q -f\n" )
  f.close()

for i in range( 1 , 11 ):
  f = open( "do" + str( i + 21 ) , "w" )
  if i < 10:
    f.write( "cirr tests.fraig/strash0" + str( i ) + ".aag\n" )
  else:
    f.write( "cirr tests.fraig/strash" + str( i ) + ".aag\n" )
  f.write( "cirp -S\n" )
  f.write( "cirp -N\n" )
  f.write( "cirp -PI\n" )
  f.write( "cirp -PO\n" )
  f.write( "cirp -FL\n" )
  f.write( "cirw\n" )
  f.write( "q -f\n" )
  f.close()

