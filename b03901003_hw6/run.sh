make clean
make
for i in {1..45}
do
  echo $i" processing..."
  ./cirTest -F "dofiles/do"$i > my.out
  ./ref/cirTest-ref -F "dofiles/do"$i > ref.out
  diff my.out ref.out
  # rm my.out
  # rm ref.out
done
