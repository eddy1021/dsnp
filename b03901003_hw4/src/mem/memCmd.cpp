/****************************************************************************
  FileName     [ memCmd.cpp ]
  PackageName  [ mem ]
  Synopsis     [ Define memory test commands ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2007-present LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/
#include <iostream>
#include <iomanip>
#include "memCmd.h"
#include "memTest.h"
#include "cmdParser.h"
#include "util.h"

using namespace std;

extern MemTest mtest;  // defined in memTest.cpp

bool
initMemCmd()
{
   if (!(cmdMgr->regCmd("MTReset", 3, new MTResetCmd) &&
         cmdMgr->regCmd("MTNew", 3, new MTNewCmd) &&
         cmdMgr->regCmd("MTDelete", 3, new MTDeleteCmd) &&
         cmdMgr->regCmd("MTPrint", 3, new MTPrintCmd)
      )) {
      cerr << "Registering \"mem\" commands fails... exiting" << endl;
      return false;
   }
   return true;
}


//----------------------------------------------------------------------
//    MTReset [(size_t blockSize)]
//----------------------------------------------------------------------
CmdExecStatus
MTResetCmd::exec(const string& option)
{
   // check option
   string token;
   if (!CmdExec::lexSingleOption(option, token))
      return CMD_EXEC_ERROR;
   if (token.size()) {
      int b;
      if (!myStr2Int(token, b) || b < (int)toSizeT(sizeof(MemTestObj))) {
         cerr << "Illegal block size (" << token << ")!!" << endl;
         return CmdExec::errorOption(CMD_OPT_ILLEGAL, token);
      }
      #ifdef MEM_MGR_H
      mtest.reset(toSizeT(b));
      #else
      mtest.reset();
      #endif // MEM_MGR_H
   }
   else
      mtest.reset();
   return CMD_EXEC_DONE;
}

void
MTResetCmd::usage(ostream& os) const
{  
   os << "Usage: MTReset [(size_t blockSize)]" << endl;
}

void
MTResetCmd::help() const
{  
   cout << setw(15) << left << "MTReset: " 
        << "(memory test) reset memory manager" << endl;
}


//----------------------------------------------------------------------
//    MTNew <(size_t numObjects)> [-Array (size_t arraySize)]
//----------------------------------------------------------------------
CmdExecStatus
MTNewCmd::exec(const string& option)
{
   // TODO
  vector<string> tokens;
  lexOptions( option , tokens );
  int b , c;
  bool arr = false;
  if( (int)tokens.size() == 0 )
    return CmdExec::errorOption( CMD_OPT_MISSING , "" );
  if( myStr2Int( tokens[0], b ) ){
    if( b <= 0 )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[0] );
    if( (int)tokens.size() > 1 ){
      if( myStrNCmp( "-Array" , tokens[1] , 2 ) )
        return CmdExec::errorOption( CMD_OPT_EXTRA , tokens[1] );
      if( (int)tokens.size() == 2 )
        return CmdExec::errorOption( CMD_OPT_MISSING , tokens[1] );
      if( !myStr2Int( tokens[2] , c ) || c <= 0 )
        return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[2] );
      if( (int)tokens.size() > 3 )
        return CmdExec::errorOption( CMD_OPT_EXTRA , tokens[3] );
      arr = true;
    }
  }else{
    if( myStrNCmp( "-Array" , tokens[0] , 2 ) )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[0] );
    if( (int)tokens.size() < 2 )
      return CmdExec::errorOption( CMD_OPT_MISSING , tokens[0] );
    if( !myStr2Int( tokens[1] , c ) || c <= 0 )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[1] );
    if( (int)tokens.size() < 3 )
      return CmdExec::errorOption( CMD_OPT_MISSING , "" );
    if( !myStr2Int( tokens[2] , b ) || b <= 0 )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[2] );
    if( (int)tokens.size() > 3 )
      return CmdExec::errorOption( CMD_OPT_EXTRA , tokens[3] );
    arr = true;
  }
  try{
    if( arr ) mtest.newArrs( b , c );
    else mtest.newObjs( b );
  }catch( bad_alloc ){
    return CMD_EXEC_ERROR;
  }
  return CMD_EXEC_DONE;
}

void
MTNewCmd::usage(ostream& os) const
{  
   os << "Usage: MTNew <(size_t numObjects)> [-Array (size_t arraySize)]\n";
}

void
MTNewCmd::help() const
{  
   cout << setw(15) << left << "MTNew: " 
        << "(memory test) new objects" << endl;
}


//----------------------------------------------------------------------
//    MTDelete <-Index (size_t objId) | -Random (size_t numRandId)> [-Array]
//----------------------------------------------------------------------
CmdExecStatus
MTDeleteCmd::exec(const string& option)
{
   // TODO
  vector<string> tokens;
  lexOptions( option , tokens );
  int b, type = -1; // 0 for -I 1 for -R
  bool arr = false;
  int rpos = -1, npos = -1;
  if( (int)tokens.size() == 0 )
    return CmdExec::errorOption( CMD_OPT_MISSING , "" );
  if( !myStrNCmp( "-Array", tokens[0], 2 ) ){
    if( (int)tokens.size() == 1 )
      return CmdExec::errorOption( CMD_OPT_MISSING , "" );
    if( !myStrNCmp( "-Index" , tokens[1], 2 ) ) type = 0;
    if( !myStrNCmp( "-Random" , tokens[1], 2 ) ) type = 1, rpos = 1;
    if( type == -1 )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[1] );
    if( (int)tokens.size() == 2 )
      return CmdExec::errorOption( CMD_OPT_MISSING , tokens[1] );
    if( !myStr2Int( tokens[2], b ) || b < type )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[2] );
    npos = 2;
    if( (int)tokens.size() > 3 )
      return CmdExec::errorOption( CMD_OPT_EXTRA , tokens[3] );
    arr = true;
  }else{
    if( !myStrNCmp( "-Index" , tokens[0] , 2 ) ) type = 0;
    if( !myStrNCmp( "-Random" , tokens[0] , 2 ) ) type = 1, rpos = 0;
    if( type == -1 )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[0] );
    if( (int)tokens.size() == 1 )
      return CmdExec::errorOption( CMD_OPT_MISSING , tokens[0] );
    if( !myStr2Int( tokens[1], b ) || b < type )
      return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[1] );
    npos = 1;
    if( (int)tokens.size() >= 3 ){
      if( !myStrNCmp( "-Array", tokens[2], 2 ) )
        arr = true;
      if( !arr ) return CmdExec::errorOption( CMD_OPT_EXTRA , tokens[2] );
      else if( (int)tokens.size() > 3 )
        return CmdExec::errorOption( CMD_OPT_EXTRA , tokens[3] );
    }
  }
  if( type == 0 ){
    if( arr ){
      if( b < (int)mtest.getArrListSize() )
        mtest.deleteArr( b );
      else{ 
        cerr << "Size of array list (" << mtest.getArrListSize() << ") is <= " << b << "!!" << endl;
        return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[ npos ] );
      }
    }else{
      if( b < (int)mtest.getObjListSize() )
        mtest.deleteObj( b );
      else{ 
        cerr << "Size of object list (" << mtest.getObjListSize() << ") is <= " << b << "!!" << endl;
        return CmdExec::errorOption( CMD_OPT_ILLEGAL , tokens[ npos ] );
      }
    }
  }else if( type == 1 ){
    for( int i = 0 ; i < b ; i ++ ){
      int tmp;
      if( arr ){
        if( (int)mtest.getArrListSize() == 0 ){ 
          cerr << "Size of array list is 0!!" << endl;
          return CmdExec::errorOption(CMD_OPT_ILLEGAL,tokens[rpos]);
        }
        tmp = rnGen( (int)mtest.getArrListSize() );
        mtest.deleteArr( tmp );
      }else{
        if( (int)mtest.getObjListSize() == 0 ){ 
          cerr << "Size of object list is 0!!" << endl;
          return CmdExec::errorOption(CMD_OPT_ILLEGAL,tokens[rpos]);
        }
        tmp = rnGen( (int)mtest.getObjListSize() );
        mtest.deleteObj( tmp );
      }
    }
  }else assert( 1 == 0 );
  return CMD_EXEC_DONE;
}

void
MTDeleteCmd::usage(ostream& os) const
{  
   os << "Usage: MTDelete <-Index (size_t objId) | "
      << "-Random (size_t numRandId)> [-Array]" << endl;
}

void
MTDeleteCmd::help() const
{  
   cout << setw(15) << left << "MTDelete: " 
        << "(memory test) delete objects" << endl;
}


//----------------------------------------------------------------------
//    MTPrint
//----------------------------------------------------------------------
CmdExecStatus
MTPrintCmd::exec(const string& option)
{
   // check option
   if (option.size())
      return CmdExec::errorOption(CMD_OPT_EXTRA, option);
   mtest.print();

   return CMD_EXEC_DONE;
}

void
MTPrintCmd::usage(ostream& os) const
{  
   os << "Usage: MTPrint" << endl;
}

void
MTPrintCmd::help() const
{  
   cout << setw(15) << left << "MTPrint: " 
        << "(memory test) print memory manager info" << endl;
}


