/****************************************************************************
  FileName     [ myHashSet.h ]
  PackageName  [ util ]
  Synopsis     [ Define HashSet ADT ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2014-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef MY_HASH_SET_H
#define MY_HASH_SET_H

#include <vector>

using namespace std;

//---------------------
// Define HashSet class
//---------------------
// To use HashSet ADT,
// the class "Data" should at least overload the "()" and "==" operators.
//
// "operator ()" is to generate the hash key (size_t)
// that will be % by _numBuckets to get the bucket number.
// ==> See "bucketNum()"
//
// "operator ()" is to check whether there has already been
// an equivalent "Data" object in the HashSet.
// Note that HashSet does not allow equivalent nodes to be inserted
//
template <class Data>
class HashSet
{
 public:
  HashSet(size_t b = 0) : _numBuckets(0), _buckets(0) { if (b != 0) init(b); }
  ~HashSet() { reset(); }

  // TODO: implement the HashSet<Data>::iterator
  // o An iterator should be able to go through all the valid Data
  //   in the Hash
  // o Functions to be implemented:
  //   - constructor(s), destructor
  //   - operator '*': return the HashNode
  //   - ++/--iterator, iterator++/--
  //   - operators '=', '==', !="
  //
  class iterator{
    friend class HashSet<Data>;

   public:
    iterator( vector<Data>* _nbuckets , int _size , int _n , int _m ) :
      nbuckets( _nbuckets ), sz( _size ), n( _n ), m( _m ) {}
    iterator( const iterator& i ):
      nbuckets( i.nbuckets ), sz( i.sz ), n( i.n ), m( i.m ) {}
    iterator(){}
    ~iterator(){}
    const Data& operator*() const{ return nbuckets[ n ][ m ]; }
    Data& operator*() { return nbuckets[ n ][ m ]; }
    iterator& operator++(){ next(); return *(this); }
    iterator operator++(int){ iterator it( *this ); next(); return it; }
    iterator& operator--(){ prev(); return *(this); }
    iterator operator--(int){ iterator it( *this ); prev(); return it; }
    iterator& operator = (const iterator& i){ 
      nbuckets = i.nbuckets;
      sz = i.sz; n = i.n; m = i.m;
      return *(this);
    }
    bool operator==(const iterator& i)const{ return n == i.n && m == i.m; }
    bool operator!=(const iterator& i)const{ return n != i.n || m != i.m; }
   private:
    void next(){
      if( m + 1 < (int)nbuckets[ n ].size() ){ m ++; return; }
      else if( m + 1 == (int)nbuckets[ n ].size() ) n ++;
      while( n < sz && (int)nbuckets[ n ].size() == 0 ) n ++;
      m = 0;
    }
    void prev(){
      if( m - 1 >= 0 ){ m --; return; }
      else n --;
      while( n >= 0 && nbuckets[ n ].size() == 0 ) n --;
      if( n >= 0 ) m = nbuckets[ n ].size() - 1;
      else m = 0;
    }
    vector<Data>* nbuckets;
    int sz, n, m;
  };

  void init(size_t b) { _numBuckets = b; _buckets = new vector<Data>[b]; }
  void reset() {
    _numBuckets = 0;
    if (_buckets) { delete [] _buckets; _buckets = 0; }
  }
  void clear() {
    for (size_t i = 0; i < _numBuckets; ++i) _buckets[i].clear();
  }
  size_t numBuckets() const { return _numBuckets; }

  vector<Data>& operator [] (size_t i) { return _buckets[i]; }
  const vector<Data>& operator [](size_t i) const { return _buckets[i]; }

  // TODO: implement these functions
  //
  // Point to the first valid data
  iterator begin()const{
    if( size() == 0 ) return end();
    iterator it( _buckets , (int)_numBuckets , 0 , -1 );
    it.next();
    return it;
  }
  // Pass the end
  iterator end() const{ return iterator( _buckets , (int)_numBuckets,
                                          (int)_numBuckets , 0 ); }
  // return true if no valid data
  bool empty() const{ return size() == 0; }
  // number of valid data
  size_t size() const{
    size_t s = 0;
    for( size_t i = 0 ; i < _numBuckets ; i ++ )
      s += _buckets[ i ].size();
    return s;
  }

  // check if d is in the hash...
  // if yes, return true;
  // else return false;
  bool check(const Data& d) const{
    size_t no = bucketNum( d );
    for( size_t i = 0 ; i < _buckets[ no ].size() ; i ++ )
      if( d == _buckets[ no ][ i ] )
        return true;
    return false;
  }

  // query if d is in the hash...
  // if yes, replace d with the data in the hash and return true;
  // else return false;
  bool query(Data& d) const{
    size_t no = bucketNum( d );
    for( size_t i = 0 ; i < _buckets[ no ].size() ; i ++ )
      if( d == _buckets[ no ][ i ] ){
        _buckets[ no ][ i ] = d;
        return true;
      }
    return false;
  }

  // update the entry in hash that is equal to d
  // if found, update that entry with d and return true;
  // else insert d into hash as a new entry and return false;
  bool update(const Data& d){
    if( query( d ) ) return true;
    _buckets[ bucketNum( d ) ].push_back( d );
    return false;
  }
  // return true if inserted successfully (i.e. d is not in the hash)
  // return false is d is already in the hash ==> will not insert
  bool insert(const Data& d){
    if( check( d ) ) return false;
    _buckets[ bucketNum( d ) ].push_back( d );
    return true;
  }

  // return true if removed successfully (i.e. d is in the hash)
  // return fasle otherwise (i.e. nothing is removed)
  bool remove(const Data& d){
    if( !check( d ) ) return false;
    size_t no = bucketNum( d ); int pos = -1;
    for( size_t i = 0 ; i < _buckets[ no ].size() ; i ++ )
      if( _buckets[ no ][ i ] == d ){
        pos = i; break;
      }
    for( int i = pos ; i < (int)_buckets[ no ].size() - 1 ; i ++ )
      _buckets[ no ][ i ] = _buckets[ no ][ i + 1 ];
    _buckets[ no ].pop_back();
    return true;
  }

 private:
  // Do not add any extra data member
  size_t            _numBuckets;
  vector<Data>*     _buckets;

  size_t bucketNum(const Data& d) const {
    return (d() % _numBuckets); }
};

#endif // MY_HASH_SET_H
