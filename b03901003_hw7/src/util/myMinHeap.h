/****************************************************************************
  FileName     [ myMinHeap.h ]
  PackageName  [ util ]
  Synopsis     [ Define MinHeap ADT ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2014-present LaDs(III), GIEE, NTU, Taiwan ]
 ****************************************************************************/

#ifndef MY_MIN_HEAP_H
#define MY_MIN_HEAP_H

#include <algorithm>
#include <vector>

template <class Data>
class MinHeap{
 public:
  MinHeap(size_t s = 0) { if (s != 0) _data.reserve(s); }
  ~MinHeap() {}

  void clear() { _data.clear(); }

  // For the following member functions,
  // We don't respond for the case vector "_data" is empty!
  const Data& operator [] (size_t i) const { return _data[i]; }   
  Data& operator [] (size_t i) { return _data[i]; }

  size_t size() const { return _data.size(); }

  // TODO
  const Data& min() const { return _data[ 0 ]; }
  void insert(const Data& d){
    _data.push_back( d );
    goUp( (int)size() - 1 );
  }
  void delMin(){
    if( size() == 0 ) return;
    _data[ 0 ] = _data[ size() - 1 ];
    _data.pop_back();
    if( size() ) goDown( 0 );
  }
  void delData(size_t i){
    _data[ i ] = _data[ size() - 1 ];
    _data.pop_back();
    if( i < size() ) goDown( goUp( i ) );
  }
 private:
  // DO NOT add or change data members
  int prt( int x ){ return ( x - 1 ) >> 1; }
  int lcd( int x ){ return ( x << 1 ) + 1; }
  int rcd( int x ){ return ( x << 1 ) + 2; }
  int goUp( int pos ){
    while( pos && _data[ pos ] < _data[ prt( pos ) ] ){
      swap( _data[ pos ] , _data[ prt( pos ) ] );
      pos = prt( pos );
    }
    return pos;
  }
  int goDown( int pos ){
    int _sz = size();
    while( lcd( pos ) < _sz ){
      int bst = lcd( pos );
      if( rcd( pos ) < _sz &&
          _data[ rcd( pos ) ] < _data[ lcd( pos ) ] ) bst = rcd( pos );
      if( _data[ bst ] < _data[ pos ] ){
        swap( _data[ bst ] , _data[ pos ] );
        pos = bst;
      }else break;
    }
    return pos;
  }
  vector<Data>   _data;
};

#endif // MY_MIN_HEAP_H
